/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: onehot
    Description: onehot source file
    Last modification: 21/04/2021
*/

import data_pkg::*;
module one_hot
#(
    parameter DW = data_pkg::N
)
(
	input  req_vector_t i_data,
	output req_vector_t o_data
);

assign o_data = ~(i_data << 1) & i_data;

endmodule