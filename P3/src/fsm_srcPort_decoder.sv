/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: uartTx_fsm
    Description: uartTx_fsm source file
    Last modification: 07/03/2021
*/
import data_pkg::*;

module fsm_srcPort_decoder
(
	input bit clk,
	input bit rst,
	input logic enb,
	input data_n_t uart_data,
	input logic Dn_ovf,
	
	output data_n_t cmd,
	output decoder_st decoder_state,
	output data_n_t src_port_register,
	output data_n_t src_port,
	output data_n_t dst_port_register,
	output data_n_t dst_port,
	output data_n_t Dn_size,
	output data_n_t Dn_register,
	output data_n_t Dn
);

decoder_st current_st, nxt_state;

always_comb begin
   case(current_st)
	   DATA_0XFE: begin
		   if((uart_data == data_n_t'('hFE)) && (enb)) begin
			   nxt_state = DATA_L;
		   end 
		   else begin
			   nxt_state = DATA_0XFE;
		   end 
	   end 
	   DATA_L: begin
		   if((uart_data >= data_n_t'('d5)) && (uart_data <= data_n_t'('d20))) begin
			   nxt_state = DATA_CMD;
		   end 
		   else begin
			   nxt_state = DATA_L;
		   end 
	   end 
	   DATA_CMD: begin
		   if(uart_data == data_n_t'('h04)) begin
			   nxt_state = DATA_SRC;
		   end 
		   else begin
			   nxt_state = DATA_CMD;
		   end 
	   end 
	   DATA_SRC: begin
		   if((uart_data >= data_n_t'('d1)) && (uart_data <= data_n_t'('d8))) begin
			   nxt_state = DATA_DST;
		   end 
		   else begin
			   nxt_state = DATA_CMD;
		   end 
	   end 
	   DATA_DST: begin
		   if((uart_data >= data_n_t'('d1)) && (uart_data <= data_n_t'('d8))) begin
			   nxt_state = DATA_M;
		   end 
		   else begin
			   nxt_state = DATA_CMD;
		   end 
	   end 
	   DATA_ST: begin
		   if(Dn_ovf) begin
			   nxt_state = DATA_0XEF;
		   end 
		   else begin
			   nxt_state = DATA_M;
		   end 
	   end 
	   DATA_0XEF: begin
		   if(uart_data == data_n_t'('hEF)) begin
			   nxt_state = DATA_0XFE;
		   end 
		   else begin
			   nxt_state = DATA_0XEF;
		   end 
	   end 
	endcase
end

always_ff@(posedge clk or negedge rst) begin // Circuito Secuenicial en un proceso always.
   if (!rst) 
      current_st  <= DATA_0XFE;
   else 
      current_st  <= nxt_state;
end

always_comb begin
   case(current_st)
	   DATA_0XFE: begin
		   cmd = 'b0;
		   decoder_state = current_st;
		   src_port_register = 1'b0;
		   src_port = 1'b0;
		   dst_port_register = 1'b0;
		   dst_port = 1'b0;
		   Dn_size = 1'b0;
		   Dn_register = 1'b0;
		   Dn = 'b0;
	   end 
	   DATA_L: begin
		   cmd = 'b0;
		   decoder_state = current_st;
		   src_port_register = 1'b0;
		   src_port = 1'b0;
		   dst_port_register = 1'b0;
		   dst_port = 1'b0;
		   Dn_size = uart_data - 'd3;
		   Dn_register = 1'b0;
		   Dn = 'b0;
	   end 
	   DATA_SRC: begin
		   cmd = 'b0;
		   decoder_state = current_st;
		   src_port_register = 1'b1;
		   src_port = uart_data;
		   dst_port_register = 1'b0;
		   dst_port = 1'b0;
		   Dn_size = 1'b0;
		   Dn_register = 1'b0;
		   Dn = 'b0;
	   end 
	   DATA_DST: begin
		   cmd = 'b0;
		   decoder_state = current_st;
		   src_port_register = 1'b0;
		   src_port = 1'b0;
		   dst_port_register = 1'b1;
		   dst_port = uart_data;
		   Dn_size = 1'b0;
		   Dn_register = 1'b0;
		   Dn = 'b0;
	   end 
	   DATA_CMD: begin
		   cmd = 'b0;
		   decoder_state = current_st;
		   src_port_register = 1'b0;
		   src_port = 1'b0;
		   dst_port_register = 1'b0;
		   dst_port = 1'b0;
		   Dn_size = 1'b0;
		   Dn_register = 1'b0;
		   Dn = 'b0;
	   end 
	   DATA_ST: begin
		   cmd = 'b0;
		   decoder_state = current_st;
		   src_port_register = 1'b0;
		   src_port = 1'b0;
		   dst_port_register = 1'b0;
		   dst_port = 1'b0;
		   Dn_size = 1'b0;
		   Dn_register = 1'b1;
		   Dn = uart_data;
	   end 
	   DATA_0XEF: begin
		   cmd = 'b0;
		   decoder_state = current_st;
		   src_port_register = 1'b0;
		   src_port = 1'b0;
		   dst_port_register = 1'b0;
		   dst_port = 1'b0;
		   Dn_size = 1'b0;
		   Dn_register = 1'b0;
		   Dn = 'b0;
	   end 
	endcase
end

endmodule 