
import port_pkg::*;
module vector_gen
(
    input  logic request_active,
    input  port_data_t dest_port,
    output port_data_t o_vect
);

assign o_vect = (request_active == 1'b1) ? (1'b1 << dest_port) : 8'd0;

endmodule