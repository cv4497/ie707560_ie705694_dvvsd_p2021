/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: request_vector_generator
    Description: request_vector_generator source file
    Last modification: 21/04/2021
*/

import data_pkg::*;

module request_vector_generator
(
	input data_n_t iport,
	output req_vector_t request_vector
);

data_n_t rvt;

assign rvt = (iport != 'd0)?((data_n_t'(1'b1)) << (iport - 'd1)):( 'b0 );

assign request_vector = req_vector_t'(rvt);
	
endmodule 