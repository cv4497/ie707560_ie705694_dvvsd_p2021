/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: uartTx_fsm
    Description: uartTx_fsm source file
    Last modification: 07/03/2021
*/
import data_pkg::*;

module fsm_transmit
(
	input bit clk,
	input bit rst,
	
	input logic enb_transmit,
	
	input logic empty_fifo0,
	input logic empty_fifo1,
	input logic empty_fifo2,
	input logic empty_fifo3,
	input logic empty_fifo4,
	input logic empty_fifo5,
	input logic empty_fifo6,
	input logic empty_fifo7,
	
	output logic pop_fifo0,
	output logic pop_fifo1,
	output logic pop_fifo2,
	output logic pop_fifo3,
	output logic pop_fifo4,
	output logic pop_fifo5,
	output logic pop_fifo6,
	output logic pop_fifo7,
	
	output selectr_e o_mux_selector
	
);

transmit_st current_st, nxt_state;
	
logic empty_ofifos;

always_comb begin
   case(current_st)
	   DATA_IDLE: begin
		   if(enb_transmit) begin
			   nxt_state = DATA_FIFO_WAIT;
		   end 
		   else begin
			   nxt_state = DATA_IDLE;
		   end 
	   end 
	   DATA_FIFO_WAIT: begin
		   nxt_state = DATA_FIFO0; 
	   end 
	   DATA_FIFO0: begin
		   if(empty_fifo0) begin
			   nxt_state = DATA_FIFO1;
		   end 
		   else begin
			   nxt_state = DATA_FIFO0;
		   end 
	   end 
	   DATA_FIFO1: begin
		   if(empty_fifo1) begin
			   nxt_state = DATA_FIFO2;
		   end 
		   else begin
			   nxt_state = DATA_FIFO1;
		   end 
	   end 
	   DATA_FIFO2: begin
		   if(empty_fifo2) begin
			   nxt_state = DATA_FIFO3;
		   end 
		   else begin
			   nxt_state = DATA_FIFO2;
		   end 
	   end 
	   DATA_FIFO3: begin
		   if(empty_fifo3) begin
			   nxt_state = DATA_FIFO4;
		   end 
		   else begin
			   nxt_state = DATA_FIFO3;
		   end 
	   end 
	   DATA_FIFO4: begin
		   if(empty_fifo4) begin
			   nxt_state = DATA_FIFO5;
		   end 
		   else begin
			   nxt_state = DATA_FIFO4;
		   end 
	   end 
	   DATA_FIFO5: begin
		   if(empty_fifo5) begin
			   nxt_state = DATA_FIFO6;
		   end 
		   else begin
			   nxt_state = DATA_FIFO5;
		   end 
	   end 
	   DATA_FIFO6: begin
		   if(empty_fifo6) begin
			   nxt_state = DATA_FIFO7;
		   end 
		   else begin
			   nxt_state = DATA_FIFO6;
		   end 
	   end 
	   DATA_FIFO7: begin
		   if(empty_fifo7) begin
			   nxt_state = DATA_IDLE;
		   end 
		   else begin
			   nxt_state = DATA_FIFO7;
		   end 
	   end 
	endcase
end

always_ff@(posedge clk or negedge rst) begin // Circuito Secuenicial en un proceso always.
   if (!rst) 
      current_st  <= DATA_IDLE;
   else 
      current_st  <= nxt_state;
end

always_comb begin
   case(current_st)
	   DATA_IDLE: begin
	   	   pop_fifo0 = 1'b0;
		   pop_fifo1 = 1'b0;
		   pop_fifo2 = 1'b0;
		   pop_fifo3 = 1'b0;
		   pop_fifo4 = 1'b0;
		   pop_fifo5 = 1'b0;
		   pop_fifo6 = 1'b0;
		   pop_fifo7 = 1'b0;
		   
		   o_mux_selector = OP_A;
	   end 
	   DATA_FIFO_WAIT: begin
	   	   pop_fifo0 = 1'b0;
		   pop_fifo1 = 1'b0;
		   pop_fifo2 = 1'b0;
		   pop_fifo3 = 1'b0;
		   pop_fifo4 = 1'b0;
		   pop_fifo5 = 1'b0;
		   pop_fifo6 = 1'b0;
		   pop_fifo7 = 1'b0;
		   
		   o_mux_selector = OP_A;
	   end 
	   DATA_FIFO0: begin
		   pop_fifo0 = 1'b1;
		   pop_fifo1 = 1'b0;
		   pop_fifo2 = 1'b0;
		   pop_fifo3 = 1'b0;
		   pop_fifo4 = 1'b0;
		   pop_fifo5 = 1'b0;
		   pop_fifo6 = 1'b0;
		   pop_fifo7 = 1'b0;
		   
		   o_mux_selector = OP_A;
	   end  
	   DATA_FIFO1: begin
		   pop_fifo0 = 1'b0;
		   pop_fifo1 = 1'b1;
		   pop_fifo2 = 1'b0;
		   pop_fifo3 = 1'b0;
		   pop_fifo4 = 1'b0;
		   pop_fifo5 = 1'b0;
		   pop_fifo6 = 1'b0;
		   pop_fifo7 = 1'b0;
		   
		   o_mux_selector = OP_B;
	   end 
	   DATA_FIFO2: begin
		   pop_fifo0 = 1'b0;
		   pop_fifo1 = 1'b0;
		   pop_fifo2 = 1'b1;
		   pop_fifo3 = 1'b0;
		   pop_fifo4 = 1'b0;
		   pop_fifo5 = 1'b0;
		   pop_fifo6 = 1'b0;
		   pop_fifo7 = 1'b0;
		   
		   o_mux_selector = OP_C;
	   end 
	   DATA_FIFO3: begin
		   pop_fifo0 = 1'b0;
		   pop_fifo1 = 1'b0;
		   pop_fifo2 = 1'b0;
		   pop_fifo3 = 1'b1;
		   pop_fifo4 = 1'b0;
		   pop_fifo5 = 1'b0;
		   pop_fifo6 = 1'b0;
		   pop_fifo7 = 1'b0;
		   
		   o_mux_selector = OP_D;
	   end 
   	   DATA_FIFO4: begin
		   pop_fifo0 = 1'b0;
		   pop_fifo1 = 1'b0;
		   pop_fifo2 = 1'b0;
		   pop_fifo3 = 1'b0;
		   pop_fifo4 = 1'b1;
		   pop_fifo5 = 1'b0;
		   pop_fifo6 = 1'b0;
		   pop_fifo7 = 1'b0;
	   	   
	   	   o_mux_selector = OP_E;
   	   end 
   	   DATA_FIFO5: begin
		   pop_fifo0 = 1'b0;
		   pop_fifo1 = 1'b0;
		   pop_fifo2 = 1'b0;
		   pop_fifo3 = 1'b0;
		   pop_fifo4 = 1'b0;
		   pop_fifo5 = 1'b1;
		   pop_fifo6 = 1'b0;
		   pop_fifo7 = 1'b0;
	   	   
	   	   o_mux_selector = OP_F;
   	   end 
   	   DATA_FIFO6: begin
		   pop_fifo0 = 1'b0;
		   pop_fifo1 = 1'b0;
		   pop_fifo2 = 1'b0;
		   pop_fifo3 = 1'b0;
		   pop_fifo4 = 1'b0;
		   pop_fifo5 = 1'b0;
		   pop_fifo6 = 1'b1;
		   pop_fifo7 = 1'b0;
	   	   
	   	   o_mux_selector = OP_G;
   	   end 
   	   DATA_FIFO7: begin
		   pop_fifo0 = 1'b0;
		   pop_fifo1 = 1'b0;
		   pop_fifo2 = 1'b0;
		   pop_fifo3 = 1'b0;
		   pop_fifo4 = 1'b0;
		   pop_fifo5 = 1'b0;
		   pop_fifo6 = 1'b0;
		   pop_fifo7 = 1'b1;
	   	   
	   	   o_mux_selector = OP_H;
	   end 
	endcase
end

assign empty_ofifos = empty_fifo0 ^ empty_fifo1 ^ empty_fifo2 ^ empty_fifo3 ^ empty_fifo4 ^ empty_fifo5 ^ empty_fifo6 ^ empty_fifo7;

endmodule 