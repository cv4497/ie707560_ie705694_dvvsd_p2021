/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title:   port decoder
    Description: fsm port control source file
    Last modification: May 5th, 2021
*/

import port_pkg::*;
module Port_Decoder
(
    input        clk,
    input        rst,
    input  port_data_t i_data,
    input  logic decoder_port_sig,
    output port_data_t dest_port,
    output port_data_t data_length
);

logic load_dest_w;
logic load_count_w;
logic next_data_sig_w;
port_data_t o_data_length_w;
port_data_t o_data_dest_w;
logic ready_w;

pipo_register
#(
	.DW(port_pkg::DW)
)   dest_port_reg
(
    .clk(clk),
    .rst(rst),
    .enb(decoder_port_sig),
    .i_data(port_pkg::DW'(i_data[7:4])),
    .o_data(o_data_dest_w)
);

pipo_register
#(
	.DW(port_pkg::DW)
)    data_length_reg
(
    .clk(clk),
    .rst(rst),
    .enb(decoder_port_sig),
    .i_data(port_pkg::DW'(i_data[3:0])),
    .o_data(o_data_length_w)
);

assign dest_port = o_data_dest_w;
assign data_length = o_data_length_w;

endmodule
