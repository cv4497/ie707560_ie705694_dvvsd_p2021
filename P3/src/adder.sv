/* 
    Author: César Villarreal @cv4497
			Luis Fernando Rodriguez @LF-RoGu
    Title: adder.sv
    Description: adder module for gray counter
    Last modification: 13/11/2020
*/

module adder
import fifo_pkg::*;
(
	input   adder_t          A,
	input	adder_t          B,
	output  adder_t          result
);

assign result = A + B;

endmodule
