/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: uartTx_fsm
    Description: uartTx_fsm source file
    Last modification: 07/03/2021
*/
import data_pkg::*;

module fsm_uart_tx
(
	input bit clk,
	input bit rst,
	input logic transmit,
	input logic ovf,
	output logic l_s,
	output logic cntr_enb,
	output logic reg_enb,
	output logic [1:0] mux_sel,
	output logic cts,
	output logic tx_active
);

uart_st current_st, nxt_state;
logic parity_bit;

always_comb begin
   case(current_st)
		IDLE_BIT: begin
			if(transmit)
				nxt_state = START_BIT;
			else 
				nxt_state = IDLE_BIT;
		end
		START_BIT: begin
			nxt_state = DATA_BIT;
		end
		DATA_BIT: begin
			if(ovf) 
				nxt_state = PARITY_BIT;
			else
				nxt_state = DATA_BIT;
		end
		PARITY_BIT: begin
			nxt_state = STOP_BIT;
		end
		STOP_BIT: begin
			nxt_state = IDLE_BIT;
		end
	endcase
end

always_ff@(posedge clk or negedge rst) begin // Circuito Secuenicial en un proceso always.
   if (!rst) 
      current_st  <= IDLE_BIT;
   else 
      current_st  <= nxt_state;
end

always_comb begin
   case(current_st)
		IDLE_BIT: begin
			cntr_enb = 1'b0;
			reg_enb = 1'b1;
			l_s = 1'b1;
			mux_sel = 2'b00;
			cts = 1'b0;
			tx_active = 1'b0;
		end
		START_BIT: begin
			cntr_enb = 1'b0;
			reg_enb = 1'b0;
			l_s = 1'b0;
			mux_sel = 2'b00;
			cts = 1'b1;
			tx_active = 1'b1;
		end
		DATA_BIT: begin
			cntr_enb = 1'b1;
			l_s = 1'b0;
			mux_sel = 2'b01;
			cts = 1'b0;
			tx_active = 1'b1;
			if(ovf) 
				reg_enb = 1'b0;
			else
				reg_enb = 1'b1;
		end
		PARITY_BIT: begin
			cntr_enb = 1'b0;
			reg_enb = 1'b0;
			l_s = 1'b0;
			mux_sel = 2'b10;
			cts = 1'b0;
			tx_active = 1'b1;
		end
		STOP_BIT: begin
			cntr_enb = 1'b0;
			reg_enb = 1'b0;
			l_s = 1'b0;
			mux_sel = 2'b11;
			cts = 1'b0;
			tx_active = 1'b1;
		end
	endcase
end

endmodule 