/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: mux_8_1
    Description: mux_8_1 source file
    Last modification: 14/02/2021
*/

import data_pkg::*;
module mux_8_1
#(
	parameter DW = data_pkg::N
)
(	
	input   logic [DW-1:0]   i_a,
	input   logic [DW-1:0]   i_b,
	input	logic [DW-1:0]	 i_c,
	input	logic [DW-1:0]	 i_d,
	input	logic [DW-1:0]	 i_e,
	input	logic [DW-1:0]	 i_f,
	input	logic [DW-1:0]	 i_g,
	input	logic [DW-1:0]	 i_h,
	input   selectr_e        i_sel,
	output  logic [DW-1:0]   o_sltd
);

always_comb begin
    case(i_sel)
    	OP_A:			 o_sltd = i_a;
	 	OP_B:			 o_sltd = i_b;
	 	OP_C:			 o_sltd = i_c;
	 	OP_D:		     o_sltd = i_d;
	    OP_E:		     o_sltd = i_e;
	    OP_F:		     o_sltd = i_f;
	    OP_G:		     o_sltd = i_g;
	    OP_H:		     o_sltd = i_h;
		default:		 o_sltd = 8'd0;
    endcase
end

endmodule
