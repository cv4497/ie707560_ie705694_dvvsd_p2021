import data_pkg::*;

module fsm_uart_rx
(
	input bit clk,
	input logic rst,
	input logic ovf,
	input logic data_in,
	
	input logic start_cntr_ovf,
	input logic data_cntr_ovf,
	
	input  logic cts,
	output logic rts,
	
	output logic enable,
	output logic bin_cntr_enb,
	output logic start_cntr_enable,
	output logic data_cntr_enable
);

uart_st current_st, nxt_state;
logic rts_t;
	
// Circuito combinacional entrada, define siguiente estado
always_comb begin
   case(current_st)
		IDLE_BIT: begin
			if((!data_in) && (!rts_t)) begin
				nxt_state = DATA_BIT;
			end
			else begin
				nxt_state = IDLE_BIT;
			end
		end
		DATA_BIT: begin
			if(ovf) begin
				nxt_state = PARITY_BIT;
			end
			else begin
				nxt_state = DATA_BIT;
			end
		end
		PARITY_BIT: begin
			nxt_state = STOP_BIT;
		end
		STOP_BIT: begin
			nxt_state = IDLE_BIT;
		end
	endcase
end

//asignaciones por default y siguiente
always_ff@(posedge clk or negedge rst) begin // Circuito Secuenicial en un proceso always.
   if (!rst) 
      current_st  <= IDLE_BIT;
   else 
      current_st  <= nxt_state;
end

always_comb begin
   case(current_st)
		IDLE_BIT: begin
			if(cts) begin
				rts_t				= 1'b0;
			end 
			enable 			= 1'b0;
			bin_cntr_enb 	= (!data_in)?(1'b1):(1'b0);
			start_cntr_enable = (!data_in)?(1'b1):(1'b0);
			data_cntr_enable = 1'b0;
		end
		DATA_BIT: begin
			enable 			= (data_cntr_ovf)?(1'b1):(1'b0);
			bin_cntr_enb 	= (data_cntr_ovf)?(1'b1):(1'b0);
			rts_t				= 1'b0;
			start_cntr_enable = 1'b0;
			data_cntr_enable = 1'b1;
		end
		PARITY_BIT: begin
			enable 			= 1'b0;
			bin_cntr_enb 	= 1'b0;
			rts_t				= 1'b0;
			start_cntr_enable = 1'b0;
			data_cntr_enable = 1'b0;
		end
		STOP_BIT: begin
			enable 			= 1'b0;
			bin_cntr_enb 	= 1'b0;
			rts_t				= 1'b1;
			start_cntr_enable = 1'b0;
			data_cntr_enable = 1'b0;
		end
	endcase
end

assign rts = rts_t;

endmodule 