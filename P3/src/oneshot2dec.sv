/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: oneshot2dec
    Description: oneshot2dec source file
    Last modification: 14/02/2021
*/

import data_pkg::*;
module oneshot2dec
#(
	parameter DW = data_pkg::N
)
(	
	input   logic [DW-1:0]   i_oneshot,
	output  logic [DW-1:0]   o_dec
);

always_comb begin
    case(i_oneshot)
    	'b0000_0001:			 o_dec = 'd0;
	 	'b0000_0010:			 o_dec = 'd1;
	 	'b0000_0100:			 o_dec = 'd2;
	 	'b0000_1000:		     o_dec = 'd3;
	    'b0001_0000:		     o_dec = 'd4;
	    'b0010_0000:		     o_dec = 'd5;
	    'b0100_0000:		     o_dec = 'd6;
	    'b1000_0000:		     o_dec = 'd7;
		default    :             o_dec = 'd0;
    endcase
end

endmodule
