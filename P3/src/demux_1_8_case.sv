/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: mux_2_1
    Description: mux_2_1 source file
    Last modification: 14/02/2021
*/

import data_pkg::*;
module demux_1_8
#(
	parameter DW = data_pkg::N
)
(	
	input   logic [DW-1:0]   	i_data,
	input   selectr_e   		i_sel,
	output  logic [DW-1:0]   	o_data_a,
	output  logic [DW-1:0]   	o_data_b,
	output  logic [DW-1:0]   	o_data_c,
	output  logic [DW-1:0]   	o_data_d,
	output  logic [DW-1:0]   	o_data_e,
	output  logic [DW-1:0]   	o_data_f,
	output  logic [DW-1:0]   	o_data_g,
	output  logic [DW-1:0]   	o_data_h
);

always_comb begin
    case(i_sel)
    OP_A: begin
        o_data_a = i_data;  
	    o_data_b = 'b0;
	    o_data_c = 'b0;
	    o_data_d = 'b0;
	    o_data_e = 'b0;
	    o_data_f = 'b0;
	    o_data_g = 'b0;
	    o_data_h = 'b0;
    end
    OP_B: begin
	    o_data_a = 'b0;
        o_data_b = i_data;   
    	o_data_c = 'b0;
	    o_data_d = 'b0;
	    o_data_e = 'b0;
	    o_data_f = 'b0;
	    o_data_g = 'b0;
	    o_data_h = 'b0;
    end
    OP_C: begin
	    o_data_a = 'b0;
	    o_data_b = 'b0;
        o_data_c = i_data;  
	    o_data_d = 'b0;
	    o_data_e = 'b0;
	    o_data_f = 'b0;
	    o_data_g = 'b0;
	    o_data_h = 'b0;
    end
    OP_D: begin
	    o_data_a = 'b0;
	    o_data_b = 'b0;
	    o_data_c = 'b0;
        o_data_d = i_data;
	    o_data_e = 'b0;
	    o_data_f = 'b0;
	    o_data_g = 'b0;
	    o_data_h = 'b0;
    end    
    OP_E: begin
	    o_data_a = 'b0;
	    o_data_b = 'b0;
	    o_data_c = 'b0;
	    o_data_d = 'b0;
        o_data_e = i_data;  
	    o_data_f = 'b0;
	    o_data_g = 'b0;
	    o_data_h = 'b0;
    end 
    OP_F: begin
	    o_data_a = 'b0;
	    o_data_b = 'b0;
	    o_data_c = 'b0;
	    o_data_d = 'b0;
	    o_data_e = 'b0;
        o_data_f = i_data;
	    o_data_g = 'b0;
	    o_data_h = 'b0;
    end 
    OP_G: begin
	    o_data_a = 'b0;
	    o_data_b = 'b0;
	    o_data_c = 'b0;
	    o_data_d = 'b0;
	    o_data_e = 'b0;
	    o_data_f = 'b0;
        o_data_g = i_data; 
	    o_data_h = 'b0;
    end 
    OP_H: begin
	    o_data_a = 'b0;
	    o_data_b = 'b0;
	    o_data_c = 'b0;
	    o_data_d = 'b0;
	    o_data_e = 'b0;
	    o_data_f = 'b0;
	    o_data_g = 'b0;
        o_data_h = i_data;  
    end 
    default: begin
	    o_data_a = 'b0;
	    o_data_b = 'b0;
	    o_data_c = 'b0;
	    o_data_d = 'b0;
	    o_data_e = 'b0;
	    o_data_f = 'b0;
	    o_data_g = 'b0;
	    o_data_h = 'b0;
    end 
    endcase
end

endmodule
