// Coder:           DSc Abisai Ramirez Perez
// Date:            31 April 2019
// Name:            port_pkg.sv
// Description:     This is the package of single-port RAM
`ifndef PORT_PKG_SV
    `define PORT_PKG_SV
package port_pkg;

    localparam  DW = 8;
    typedef logic [DW-1:0]       port_data_t;

    typedef enum logic [3:0]
    {
        IDLE1_PORT_DEC_ST,
        DEST_PORT_DEC_ST,
        IDLE2_PORT_DEC_ST,
        DLEN_PORT_DEC_ST
    } fsm_decoder_st;

    typedef enum logic [14:0]
    {
        PORT_DISABLED,
        PORT_WAIT_CMD,
        PORT_FETCH_CMD,
        PORT_DECODE,
        PORT_IDLE_DATA,
        PORT_WAIT_DATA,
        PORT_FETCH_DATA,
        PORT_VER_FULL,
        PORT_WAIT_ARB,
        PORT_GET_LEN1,
        PORT_GET_LEN2,
        PORT_SEND_DATA1,
        PORT_SEND_DATA2,
        PORT_ARB_ACK,
        PORT_WAIT_FIFO_CMD,
        RESTORE_POINTERS_ST,
        WAIT_FOR_DATA
    }fsm_port_ctrl_st;

    /*
       PORT_DISABLED: port is waiting to be activated
       
    */

    
endpackage
`endif
