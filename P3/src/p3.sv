import data_pkg::*, port_pkg::*;

module p3
(
	input bit clk,
	input bit pll_clk,
	input logic rst,
	
	input  logic serial_tx2rx, //parallel data from uart2uart
	
	input logic cts,
	output logic rts,
	output port_data_t o_data
);

data_n_t data_rx;
	
data_n_t cmd, src_portLenght, src_port;

data_n_t cmd_port;

logic Dn_fifo, cmd_fifo;
logic data_processing;
logic transmit_processing;
logic retransmit_processing;
	

uart_module
uart_module
(
	.clk(clk),
	.o_clk(pll_clk),
	.rst(rst),
	.serial_tx2rx(serial_tx2rx),
	.cts(cts),
	
	.rts(rts),
	.data(data_rx),
	.data_load(),
	.dst_register(),
	
	.cmd(cmd),
	.src_portLenght(src_portLenght),
	
	.src_port(src_port),
	.dst_port(),
	.cmd_port(cmd_port),
	.Dn_port(),
	
	.dst_fifo(),
	.Dn_fifo(Dn_fifo),
	.cmd_fifo(cmd_fifo)
);

qsys
qsys_module
(
	.clk(clk),
	.rst(rst),
	.i_data(data_rx),
	.i_cmd(cmd_port),
	.load(cmd_fifo),
	.iFIFO_data_push(Dn_fifo),
	.num_iports(src_portLenght),
	.src_port(src_port),
	.data_processing(data_processing),
	.transmit_processing(transmit_processing),
	.retransmit_processing(retransmit_processing),
	.data_processing_finished(),
	.transmit_finished(),
	.o_data(o_data)
);

assign transmit_processing = (cmd == 'h06);
assign retransmit_processing = (cmd == 'h02);
assign data_processing = (cmd == 'h05)?(1'b1):(1'b0);

endmodule 
