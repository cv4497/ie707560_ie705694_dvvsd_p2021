module piso_register_lsb
#(
	parameter DW = data_pkg::N
)
(
	input bit clk,
	input logic rst,
	input logic enb,
	input logic l_s,
	input logic [DW-1:0] i_data,
	output logic o_data
);

logic [DW-1:0] rgstr_r;
logic [DW-1:0] rgstr_next;

always_comb begin
	if(l_s) 
		rgstr_next = i_data; 	//load data to register
	else 
		rgstr_next = {rgstr_r[0], rgstr_r[DW-1:1]}; //shift
end

always_ff@(posedge clk or negedge rst)  begin: rgstr_label
   if(!rst)
        rgstr_r  <= '0;
	else if(enb)
		rgstr_r  <= rgstr_next;
end:rgstr_label

assign o_data = rgstr_r[0];

endmodule 