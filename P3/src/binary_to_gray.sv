module binary_to_gray
import fifo_pkg::*;
(
    input  gray_count_t binary,
    output gray_count_t gray
);

/* binary to gray conversion */
assign gray[W_ADDR-1:0] = binary[W_ADDR-1:0]^{1'b0,binary[W_ADDR-1:1]};

endmodule