/* 
    Author: César Villarreal @cv4497
			Luis Fernando Rodriguez @LF-RoGu
    Title: adder.sv
    Description: adder module for gray counter
    Last modification: 13/11/2020
*/
import data_pkg::*;
module transmit
(
	input bit clk,
	input logic rst,
	input logic enb_transmit,
	
	input logic empty_fifo0,
	input logic empty_fifo1,
	input logic empty_fifo2,
	input logic empty_fifo3,
	input logic empty_fifo4,
	input logic empty_fifo5,
	input logic empty_fifo6,
	input logic empty_fifo7,
	
	output logic pop_fifo0,
	output logic pop_fifo1,
	output logic pop_fifo2,
	output logic pop_fifo3,
	output logic pop_fifo4,
	output logic pop_fifo5,
	output logic pop_fifo6,
	output logic pop_fifo7,
	
	output selectr_e o_mux_selector
);

logic pop_fifo0_w, pop_fifo1_w, pop_fifo2_w, pop_fifo3_w, pop_fifo4_w, pop_fifo5_w, pop_fifo6_w, pop_fifo7_w;

fsm_transmit
transmit_control
(
	.clk(clk),
	.rst(rst),
	
	.enb_transmit(enb_transmit),
	
	.empty_fifo0(empty_fifo0),
	.empty_fifo1(empty_fifo1),
	.empty_fifo2(empty_fifo2),
	.empty_fifo3(empty_fifo3),
	.empty_fifo4(empty_fifo4),
	.empty_fifo5(empty_fifo5),
	.empty_fifo6(empty_fifo6),
	.empty_fifo7(empty_fifo7),
	
	.pop_fifo0(pop_fifo0_w),
	.pop_fifo1(pop_fifo1_w),
	.pop_fifo2(pop_fifo2_w),
	.pop_fifo3(pop_fifo3_w),
	.pop_fifo4(pop_fifo4_w),
	.pop_fifo5(pop_fifo5_w),
	.pop_fifo6(pop_fifo6_w),
	.pop_fifo7(pop_fifo7_w),
	
	.o_mux_selector(o_mux_selector)
);


state_switch
swith0
(
	.clk(clk),
	.rst(rst),
	.enb(pop_fifo0_w),
	.rst_module(empty_fifo0),
	.o_sltd(pop_fifo0)
);

state_switch
swith1
(
	.clk(clk),
	.rst(rst),
	.enb(pop_fifo1_w),
	.rst_module(empty_fifo1),
	.o_sltd(pop_fifo1)
);

state_switch
swith2
(
	.clk(clk),
	.rst(rst),
	.enb(pop_fifo2_w),
	.rst_module(empty_fifo2),
	.o_sltd(pop_fifo2)
);
	
state_switch
swith3
(
	.clk(clk),
	.rst(rst),
	.enb(pop_fifo3_w),
	.rst_module(empty_fifo3),
	.o_sltd(pop_fifo3)
);
	
state_switch
swith4
(
	.clk(clk),
	.rst(rst),
	.enb(pop_fifo4_w),
	.rst_module(empty_fifo4),
	.o_sltd(pop_fifo4)
);
	
state_switch
swith5
(
	.clk(clk),
	.rst(rst),
	.enb(pop_fifo5_w),
	.rst_module(empty_fifo5),
	.o_sltd(pop_fifo5)
);
	
state_switch
swith6
(
	.clk(clk),
	.rst(rst),
	.enb(pop_fifo6_w),
	.rst_module(empty_fifo6),
	.o_sltd(pop_fifo6)
);

state_switch
swith7
(
	.clk(clk),
	.rst(rst),
	.enb(pop_fifo7_w),
	.rst_module(empty_fifo7),
	.o_sltd(pop_fifo7)
);
endmodule
