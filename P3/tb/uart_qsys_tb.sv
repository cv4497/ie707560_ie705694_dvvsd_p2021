`timescale 1ns / 1ps
module uart_qsys_tb();


localparam PERIOD = 2;

/*************/
import port_pkg::*, fifo_pkg::*, data_pkg::*, uart_qsys_tester::*;

/* module ports */
logic clk;
logic rst;
uart_qsys_if qsysif(clk,rst);

p3 p3_mod
(
    .clk(clk),
    .rst(rst),
    .data_tx(qsysif.data_tx),
    .transmit(qsysif.transmit),
    .rts(qsysif.rts),
    .o_data(qsysif.o_data)
);

localparam array_size = 16;
localparam DW_G= 8;

uart_qsys_tester tester = new(qsysif);
int i;

initial begin
    reset();
    tester.init_signals();
    /* set input ports */
    tester.command_0x01(8'd8);
    //tester.send_data_to_all_ports();
    //tester.send_data_to_all_to_dest_port(4'd2);
    tester.fill_all_oports();
    
    /* start processing */
    tester.command_0x05();
end

always begin
    #(PERIOD/2) clk<= ~clk;
end

task reset();
    clk = 1'b0;
    rst = 1'b1;

    #PERIOD rst = 1'b0;
    #PERIOD rst = 1'b1;
endtask

endmodule
