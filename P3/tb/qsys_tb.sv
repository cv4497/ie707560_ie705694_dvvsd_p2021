`timescale 1ns / 1ps
module qsys_tb();


localparam PERIOD = 2;

/*************/
import port_pkg::*, fifo_pkg::*, data_pkg::*, qsys_tester::*;

/* module ports */
logic clk;
logic rst;
qsys_if qsysif(clk,rst);

qsys qsys_mod
(
    .clk(clk),
    .rst(rst),
    .i_data(qsysif.i_data),
    .i_cmd(qsysif.i_cmd),
    .load(qsysif.load),
    .iFIFO_data_push(qsysif.iFIFO_data_push),
    .num_iports(qsysif.num_iports),
    .src_port(qsysif.src_port),
    .data_processing(qsysif.data_processing),
    .transmit_processing(qsysif.transmit_processing),
    .retransmit_processing(qsysif.retransmit_processing),
    .data_processing_finished(qsysif.data_processing_finished),
    .transmit_finished(qsysif.transmit_finished),
    .o_data(qsysif.o_data)
);

localparam array_size = 16;
localparam DW_G= 8;

qsys_tester tester = new(qsysif);
int i;

initial begin
    reset();
    tester.init_signals();
    /* set input ports */
    tester.command_0x01(8'd8);
    //tester.send_data_to_all_ports();
    //tester.send_data_to_all_to_dest_port(4'd2);
    tester.fill_test();
    
    /* start processing */
    tester.command_0x05();


    tester.command_0x06();
end

always begin
    #(PERIOD/2) clk<= ~clk;
end

task reset();
    clk = 1'b0;
    rst = 1'b1;

    #PERIOD rst = 1'b0;
    #PERIOD rst = 1'b1;
endtask

endmodule
