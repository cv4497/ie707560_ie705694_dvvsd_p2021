
import port_pkg::*;
import fifo_pkg::*;
import data_pkg::*;

interface uart_qsys_if (
    input clk,
    input rst
);
    port_data_t data_tx;
	logic transmit;
    logic rts;
    port_data_t o_data;

    modport ports
    (
	    input clk,
	    input rst,
	    input data_tx,
	    input transmit, 
	    
	    output rts,
	    output o_data
    );

endinterface