
`timescale 1ns / 1ps
module iport_tb();

localparam PERIOD = 2;

/*************/
import port_pkg::*;
import fifo_pkg::*;
import data_pkg::*;

/* module ports */
logic clk;
logic rst;
port_data_t [7:0] i_data;
port_data_t [7:0] i_cmd;
logic [7:0] port_enable;
logic [7:0] execute_req;
logic [7:0] load;
logic [7:0] iFIFO_data_push;
/*outs */
fsm_port_ctrl_st [7:0] port_state;
logic [7:0] port_full;
logic [7:0] port_empty;
port_data_t [7:0] o_data;
logic [7:0] arb_ack;
logic [7:0] cmd_ready;
logic [7:0] port_ready;
port_data_t [7:0] port_vector;
port_data_t [7:0] dest_port;
port_data_t [7:0] data_length;

/* module ports */
iport iport_0
(
    /*ins*/
    .clk(clk),
    .rst(rst),
    .i_cmd(i_cmd[0]),
    .i_data(i_data[0]),
    .port_number(8'b0),
    .port_enable(port_enable[0]),
    .execute_req(execute_req[0]),
    .load(load[0]),
    .iFIFO_data_push(iFIFO_data_push[0]),
    /*outs */
    .port_state(port_state[0]),
    .port_full(port_full[0]),
    .port_empty(port_empty[0]),
    .o_data(o_data[0]),
    .arb_ack(arb_ack[0]),
    .cmd_ready(cmd_ready[0]),
    .port_ready(port_ready[0]),
    .dest_port(dest_port[0]),
    .port_vector(port_vector[0])
);

// wcrr arbeiter_0
// (
//     .clk(clk),
//     .rst(rst),
//     .ack(arb_ack),
//     .request_vector(),
//     .valid(),
//     .grant()
// );

/*************/
// INPUTS:
// i_data
// port_enable 
// execute_req
// load 
/*************/
logic [3:0] dest;
logic [3:0] length;
logic [7:0] inp_data;
initial begin
    reset();
    enable_port(0);

    dest = 4'd2;
    length = 4'd10;
    set_dest_and_length(dest, length, 0);
    transfer_ififo_data(length, 0);

    dest = 4'd1;
    length = 4'd6;
    set_dest_and_length(dest, length, 0);
    transfer_ififo_data(length, 0);

    dest = 4'd1;
    length = 4'd5;
    set_dest_and_length(dest, length, 0);
    transfer_ififo_data(length, 0);

    wait(port_state[0] == PORT_WAIT_ARB);
    transmit(0);


    wait(port_state[0] == PORT_WAIT_ARB);
    transmit(0);

end

always begin
    #(PERIOD/2) clk<= ~clk;
end

task enable_port(input logic [3:0] port);
@(posedge clk)
    #PERIOD port_enable[port] = 1'b1;
    //#PERIOD port_enable = 1'b0;
endtask


task set_dest_and_length(input logic [3:0] dest, input logic [3:0]  length, input logic [3:0] port);
//@(posedge clk)
    $display(dest);
    #PERIOD i_cmd[port] = {dest,length-4'd1};
    load_data(port);
endtask

int i;
task transfer_ififo_data(input logic [3:0]  length, input logic [3:0] port);
    for(i=0; i < length; i++) begin
        #PERIOD i_data[port] = i;
        #PERIOD iFIFO_data_push[port] = 1'b1;
        #PERIOD iFIFO_data_push[port] = 1'b0;
    end
endtask

task load_data(input logic [3:0] port);
    #PERIOD load[port] = 1'b1;
    #PERIOD load[port] = 1'b0;
endtask

task transmit(input logic [3:0] port);
    #PERIOD execute_req[port] = 1'b1;
    #PERIOD execute_req[port] = 1'b0;
endtask

task reset();
	clk = 1'b0;
	rst = 1'b1;
    i_cmd[0] = 8'd0;
    port_enable[0] = 1'b0;
    execute_req[0] = 1'b0;
    load[0] = 1'b0;
    iFIFO_data_push[0] = 1'b0;
	#PERIOD rst = 1'b1;
	#PERIOD rst = 1'b0;
	#PERIOD rst = 1'b1;
endtask

//delay
task delay(int dly = 1);
    repeat(dly)
        @(posedge clk);
endtask

endmodule
