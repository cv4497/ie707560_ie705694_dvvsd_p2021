onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label clk /qsys_tb/clk
add wave -noupdate -label rst /qsys_tb/rst
add wave -noupdate -label i_data -radix ascii /qsys_tb/qsys_mod/i_data
add wave -noupdate -label i_cmd -radix unsigned /qsys_tb/qsys_mod/i_cmd
add wave -noupdate -label load /qsys_tb/qsys_mod/load
add wave -noupdate -label iFIFO_data_push /qsys_tb/qsys_mod/iFIFO_data_push
add wave -noupdate -label num_iports -radix unsigned /qsys_tb/qsys_mod/num_iports
add wave -noupdate -label src_port -radix unsigned /qsys_tb/qsys_mod/src_port
add wave -noupdate -label data_processing /qsys_tb/qsys_mod/data_processing
add wave -noupdate -label o_data -radix ascii /qsys_tb/qsys_mod/o_data
add wave -noupdate /qsys_tb/qsys_mod/error
add wave -noupdate -group {SRC PORT DATA} -label i_data -radix ascii /qsys_tb/qsys_mod/src_port_data/i_data
add wave -noupdate -group {SRC PORT DATA} -label i_sel -radix unsigned /qsys_tb/qsys_mod/src_port_data/i_sel
add wave -noupdate -group {SRC PORT DATA} -label o_data_a -radix unsigned /qsys_tb/qsys_mod/src_port_data/o_data_a
add wave -noupdate -group {SRC PORT DATA} -label o_data_b -radix unsigned /qsys_tb/qsys_mod/src_port_data/o_data_b
add wave -noupdate -group {SRC PORT DATA} -label o_data_c -radix unsigned /qsys_tb/qsys_mod/src_port_data/o_data_c
add wave -noupdate -group {SRC PORT DATA} -label o_data_d -radix unsigned /qsys_tb/qsys_mod/src_port_data/o_data_d
add wave -noupdate -group {SRC PORT DATA} -label o_data_e -radix unsigned /qsys_tb/qsys_mod/src_port_data/o_data_e
add wave -noupdate -group {SRC PORT DATA} -label o_data_f -radix unsigned /qsys_tb/qsys_mod/src_port_data/o_data_f
add wave -noupdate -group {SRC PORT DATA} -label o_data_g -radix unsigned /qsys_tb/qsys_mod/src_port_data/o_data_g
add wave -noupdate -group {SRC PORT DATA} -label o_data_h -radix unsigned /qsys_tb/qsys_mod/src_port_data/o_data_h
add wave -noupdate -group {SRC PORT CMD} -label i_data -radix unsigned /qsys_tb/qsys_mod/src_port_cmd/i_data
add wave -noupdate -group {SRC PORT CMD} -label i_sel -radix unsigned /qsys_tb/qsys_mod/src_port_cmd/i_sel
add wave -noupdate -group {SRC PORT CMD} -label o_data_a -radix unsigned /qsys_tb/qsys_mod/src_port_cmd/o_data_a
add wave -noupdate -group {SRC PORT CMD} -label o_data_b -radix unsigned /qsys_tb/qsys_mod/src_port_cmd/o_data_b
add wave -noupdate -group {SRC PORT CMD} -label o_data_c -radix unsigned /qsys_tb/qsys_mod/src_port_cmd/o_data_c
add wave -noupdate -group {SRC PORT CMD} -label o_data_d -radix unsigned /qsys_tb/qsys_mod/src_port_cmd/o_data_d
add wave -noupdate -group {SRC PORT CMD} -label o_data_e -radix unsigned /qsys_tb/qsys_mod/src_port_cmd/o_data_e
add wave -noupdate -group {SRC PORT CMD} -label o_data_f -radix unsigned /qsys_tb/qsys_mod/src_port_cmd/o_data_f
add wave -noupdate -group {SRC PORT CMD} -label o_data_g -radix unsigned /qsys_tb/qsys_mod/src_port_cmd/o_data_g
add wave -noupdate -group {SRC PORT CMD} -label o_data_h -radix unsigned /qsys_tb/qsys_mod/src_port_cmd/o_data_h
add wave -noupdate -group {SRC PORT LOAD} -label i_data /qsys_tb/qsys_mod/src_port_load/i_data
add wave -noupdate -group {SRC PORT LOAD} -label i_sel /qsys_tb/qsys_mod/src_port_load/i_sel
add wave -noupdate -group {SRC PORT LOAD} -label o_data_a /qsys_tb/qsys_mod/src_port_load/o_data_a
add wave -noupdate -group {SRC PORT LOAD} -label o_data_b /qsys_tb/qsys_mod/src_port_load/o_data_b
add wave -noupdate -group {SRC PORT LOAD} -label o_data_c /qsys_tb/qsys_mod/src_port_load/o_data_c
add wave -noupdate -group {SRC PORT LOAD} -label o_data_d /qsys_tb/qsys_mod/src_port_load/o_data_d
add wave -noupdate -group {SRC PORT LOAD} -label o_data_e /qsys_tb/qsys_mod/src_port_load/o_data_e
add wave -noupdate -group {SRC PORT LOAD} -label o_data_f /qsys_tb/qsys_mod/src_port_load/o_data_f
add wave -noupdate -group {SRC PORT LOAD} -label o_data_g /qsys_tb/qsys_mod/src_port_load/o_data_g
add wave -noupdate -group {SRC PORT LOAD} -label o_data_h /qsys_tb/qsys_mod/src_port_load/o_data_h
add wave -noupdate -group {SRC PORT DATA PUSH} -label i_data /qsys_tb/qsys_mod/src_port_fifo_push/i_data
add wave -noupdate -group {SRC PORT DATA PUSH} -label i_sel /qsys_tb/qsys_mod/src_port_fifo_push/i_sel
add wave -noupdate -group {SRC PORT DATA PUSH} -label o_data_a /qsys_tb/qsys_mod/src_port_fifo_push/o_data_a
add wave -noupdate -group {SRC PORT DATA PUSH} -label o_data_b /qsys_tb/qsys_mod/src_port_fifo_push/o_data_b
add wave -noupdate -group {SRC PORT DATA PUSH} -label o_data_c /qsys_tb/qsys_mod/src_port_fifo_push/o_data_c
add wave -noupdate -group {SRC PORT DATA PUSH} -label o_data_d /qsys_tb/qsys_mod/src_port_fifo_push/o_data_d
add wave -noupdate -group {SRC PORT DATA PUSH} -label o_data_e /qsys_tb/qsys_mod/src_port_fifo_push/o_data_e
add wave -noupdate -group {SRC PORT DATA PUSH} -label o_data_f /qsys_tb/qsys_mod/src_port_fifo_push/o_data_f
add wave -noupdate -group {SRC PORT DATA PUSH} -label o_data_g /qsys_tb/qsys_mod/src_port_fifo_push/o_data_g
add wave -noupdate -group {SRC PORT DATA PUSH} -label o_data_h /qsys_tb/qsys_mod/src_port_fifo_push/o_data_h
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {13 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 469
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {8 ps}
