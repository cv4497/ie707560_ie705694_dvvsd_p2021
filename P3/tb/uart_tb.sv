`timescale 1ns / 1ps
module uart_tb();
import data_pkg::*;

localparam PERIOD = 2;

logic clk;
logic rst;
logic transmit;

logic cts;

logic rx_isr;

data_n_t data_tx;
data_n_t data_rx;

uart uut
(
    .clk(clk),
    .rst(rst),
    .transmit(transmit),
	
	.data_tx(data_rx),
    .data_rx(data_tx),
	
	.octs(cts),
	
	.rx_interrupt(rx_isr),
	.parity_error()
);

initial begin
	clk = 1'd0;
	rst = 1'd1;
	transmit = 1'b0;
	
	#PERIOD rst = 1'b1;
	#PERIOD rst = 1'b0;
	#PERIOD rst = 1'b1;
	
	#PERIOD; #PERIOD; #PERIOD
	
	data2send('d156);
	@(rx_isr == 1)
	data2send('d10);
	@(rx_isr == 1)
	data2send('d7);
	@(rx_isr == 1)
	data2send('d128);

end

always begin
    #(PERIOD/2) clk<= ~clk;
end

task data2send(data_n_t data);
	#PERIOD data_rx = data;
	#PERIOD transmit = 1'b0;
	#PERIOD transmit = 1'b1;
	
	@(posedge cts)
	
	#PERIOD transmit = 1'b0;
	
	delay(60);
	
endtask

//delay
task delay(int dly = 1);
    repeat(dly)
        @(posedge clk);
endtask
endmodule
