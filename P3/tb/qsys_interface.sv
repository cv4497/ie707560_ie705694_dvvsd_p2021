
import port_pkg::*;
import fifo_pkg::*;
import data_pkg::*;

interface qsys_if (
    input clk,
    input rst
);

port_data_t i_data;
port_data_t i_cmd;
port_data_t o_data;
port_data_t num_iports;
port_data_t src_port;
logic load;
logic iFIFO_data_push;
logic data_processing;
logic transmit_processing;
logic retransmit_processing;
logic data_processing_finished;
logic transmit_finished;

modport ports
(
    input clk,
    input rst,
    input i_data,
    input i_cmd,
    input num_iports,
    input src_port,
    input load,
    input iFIFO_data_push,
    input data_processing,
    input transmit_processing,
    input retransmit_processing,
    output data_processing_finished,
    output transmit_finished,
    output o_data
);

endinterface