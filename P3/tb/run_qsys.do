if [file exists work] {vdel -all}
vlib work
vlog -f files_qsys.f
onbreak {resume}
set NoQuitOnFinish 1
vsim -voptargs=+acc work.qsys_tb
do wave_qsys.do
run 3 us