onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -color Firebrick -label clk /port_decoder_tb/uut/clk
add wave -noupdate -color Gray55 -label rst /port_decoder_tb/uut/rst
add wave -noupdate -label i_data -radix unsigned /port_decoder_tb/uut/i_data
add wave -noupdate -color {Cornflower Blue} -label decoder_port_sig /port_decoder_tb/uut/decoder_port_sig
add wave -noupdate -color Gold -label dest_port /port_decoder_tb/uut/dest_port
add wave -noupdate -color Gold -label data_length /port_decoder_tb/uut/data_length
add wave -noupdate -color Gold -label next_data_sig /port_decoder_tb/uut/next_data_sig
add wave -noupdate -expand -group FSM_Port_Decoder -color {Cornflower Blue} -label start /port_decoder_tb/uut/FSM_Port_Decoder/start
add wave -noupdate -expand -group FSM_Port_Decoder -color Gold -label load_dest /port_decoder_tb/uut/FSM_Port_Decoder/load_dest
add wave -noupdate -expand -group FSM_Port_Decoder -color Gold -label load_count /port_decoder_tb/uut/FSM_Port_Decoder/load_count
add wave -noupdate -expand -group FSM_Port_Decoder -color Gold -label next_data_sig /port_decoder_tb/uut/FSM_Port_Decoder/next_data_sig
add wave -noupdate -expand -group FSM_Port_Decoder -label current_st /port_decoder_tb/uut/FSM_Port_Decoder/current_st
add wave -noupdate -expand -group {Dest Port Register} -label enb /port_decoder_tb/uut/dest_port_reg/enb
add wave -noupdate -expand -group {Dest Port Register} -label i_data -radix unsigned /port_decoder_tb/uut/dest_port_reg/i_data
add wave -noupdate -expand -group {Dest Port Register} -label o_data -radix unsigned /port_decoder_tb/uut/dest_port_reg/o_data
add wave -noupdate -expand -group {Data Length Register} -label enb /port_decoder_tb/uut/data_length_reg/enb
add wave -noupdate -expand -group {Data Length Register} -label i_data -radix unsigned /port_decoder_tb/uut/data_length_reg/i_data
add wave -noupdate -expand -group {Data Length Register} -label o_data -radix unsigned /port_decoder_tb/uut/data_length_reg/o_data
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {5798 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 348
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {44640 ps}
