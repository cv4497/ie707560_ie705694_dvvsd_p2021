onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /p3_tb/p3_top/clk
add wave -noupdate /p3_tb/p3_top/pll_clk
add wave -noupdate -divider {UART PC}
add wave -noupdate /p3_tb/p3_top/rts
add wave -noupdate -divider P3
add wave -noupdate -divider P3
add wave -noupdate -divider P3
add wave -noupdate -divider {UART MODULE}
add wave -noupdate -divider {UART MODULE}
add wave -noupdate -radix ascii /p3_tb/p3_top/uart_module/data
add wave -noupdate -group {uart rx} -group {fsm rx} /p3_tb/p3_top/uart_module/uart_rx_top/fsm_cntrl_rx_top/clk
add wave -noupdate -group {uart rx} -group {fsm rx} /p3_tb/p3_top/uart_module/uart_rx_top/fsm_cntrl_rx_top/rst
add wave -noupdate -group {uart rx} -group {fsm rx} /p3_tb/p3_top/uart_module/uart_rx_top/fsm_cntrl_rx_top/ovf
add wave -noupdate -group {uart rx} -group {fsm rx} /p3_tb/p3_top/uart_module/uart_rx_top/fsm_cntrl_rx_top/data_in
add wave -noupdate -group {uart rx} -group {fsm rx} /p3_tb/p3_top/uart_module/uart_rx_top/fsm_cntrl_rx_top/cts
add wave -noupdate -group {uart rx} -group {fsm rx} /p3_tb/p3_top/uart_module/uart_rx_top/fsm_cntrl_rx_top/rts
add wave -noupdate -group {uart rx} -group {fsm rx} /p3_tb/p3_top/uart_module/uart_rx_top/fsm_cntrl_rx_top/stop_sequence
add wave -noupdate -group {uart rx} -group {fsm rx} /p3_tb/p3_top/uart_module/uart_rx_top/fsm_cntrl_rx_top/data_cntr_enable
add wave -noupdate -group {uart rx} -group {fsm rx} /p3_tb/p3_top/uart_module/uart_rx_top/fsm_cntrl_rx_top/current_st
add wave -noupdate -group {uart rx} -group {fsm rx} /p3_tb/p3_top/uart_module/uart_rx_top/fsm_cntrl_rx_top/nxt_state
add wave -noupdate -group {uart rx} -group {fsm rx} /p3_tb/p3_top/uart_module/uart_rx_top/fsm_cntrl_rx_top/rts_t
add wave -noupdate -group {uart rx} -group sipo /p3_tb/p3_top/uart_module/uart_rx_top/sipo_register_msb_uartRX/clk
add wave -noupdate -group {uart rx} -group sipo /p3_tb/p3_top/uart_module/uart_rx_top/sipo_register_msb_uartRX/rst
add wave -noupdate -group {uart rx} -group sipo /p3_tb/p3_top/uart_module/uart_rx_top/sipo_register_msb_uartRX/enb
add wave -noupdate -group {uart rx} -group sipo /p3_tb/p3_top/uart_module/uart_rx_top/sipo_register_msb_uartRX/data_in
add wave -noupdate -group {uart rx} -group sipo -radix hexadecimal /p3_tb/p3_top/uart_module/uart_rx_top/sipo_register_msb_uartRX/data_out
add wave -noupdate -group {uart rx} -group {bin counter} /p3_tb/p3_top/uart_module/uart_rx_top/bin_counter_uartRX/enb
add wave -noupdate -group {uart rx} -group {bin counter} /p3_tb/p3_top/uart_module/uart_rx_top/bin_counter_uartRX/max_count
add wave -noupdate -group {uart rx} -group {bin counter} /p3_tb/p3_top/uart_module/uart_rx_top/bin_counter_uartRX/ovf
add wave -noupdate -group {uart rx} -group {bin counter} -radix decimal /p3_tb/p3_top/uart_module/uart_rx_top/bin_counter_uartRX/count
add wave -noupdate -group {uart rx} -group {data counter} /p3_tb/p3_top/uart_module/uart_rx_top/data_counter_uartRx/enb
add wave -noupdate -group {uart rx} -group {data counter} /p3_tb/p3_top/uart_module/uart_rx_top/data_counter_uartRx/max_count
add wave -noupdate -group {uart rx} -group {data counter} /p3_tb/p3_top/uart_module/uart_rx_top/data_counter_uartRx/ovf
add wave -noupdate -group {uart rx} -group {data counter} -radix decimal /p3_tb/p3_top/uart_module/uart_rx_top/data_counter_uartRx/count
add wave -noupdate -group {uart rx} -expand -group {rx register} /p3_tb/p3_top/uart_module/uart_rx_top/uart_data_register/enb
add wave -noupdate -group {uart rx} -expand -group {rx register} -radix ascii /p3_tb/p3_top/uart_module/uart_rx_top/uart_data_register/i_data
add wave -noupdate -group {uart rx} -expand -group {rx register} -radix ascii /p3_tb/p3_top/uart_module/uart_rx_top/uart_data_register/o_data
add wave -noupdate -group decoder -group {rts detector} /p3_tb/p3_top/uart_module/seq_decode/rts_detector/enb
add wave -noupdate -group decoder -group {rts detector} /p3_tb/p3_top/uart_module/seq_decode/rts_detector/i_data
add wave -noupdate -group decoder -group {rts detector} /p3_tb/p3_top/uart_module/seq_decode/rts_detector/o_data
add wave -noupdate -group decoder -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/decoder_data_ovf
add wave -noupdate -group decoder -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/valid_data
add wave -noupdate -group decoder -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/data_load
add wave -noupdate -group decoder -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/cmd_register
add wave -noupdate -group decoder -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/lenght_register
add wave -noupdate -group decoder -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/src_portLenght_register
add wave -noupdate -group decoder -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/src_register
add wave -noupdate -group decoder -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/dst_register
add wave -noupdate -group decoder -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/data_counter_enb
add wave -noupdate -group decoder -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/dst_fifo
add wave -noupdate -group decoder -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/Dn_fifo
add wave -noupdate -group decoder -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/current_st
add wave -noupdate -group decoder -group {fsm decoder} /p3_tb/p3_top/uart_module/seq_decode/decoder/nxt_state
add wave -noupdate -group decoder -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_0xfe
add wave -noupdate -group decoder -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_l
add wave -noupdate -group decoder -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_cmd_01
add wave -noupdate -group decoder -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_cmd_02
add wave -noupdate -group decoder -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_cmd_03
add wave -noupdate -group decoder -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_cmd_04
add wave -noupdate -group decoder -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_cmd_05
add wave -noupdate -group decoder -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_cmd_06
add wave -noupdate -group decoder -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_m
add wave -noupdate -group decoder -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_n
add wave -noupdate -group decoder -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_src
add wave -noupdate -group decoder -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_dst
add wave -noupdate -group decoder -group {fsm decoder} -group flags /p3_tb/p3_top/uart_module/seq_decode/decoder/uart_oxef
add wave -noupdate -group decoder -group {cmd detector} /p3_tb/p3_top/uart_module/seq_decode/cmd_detector/enb
add wave -noupdate -group decoder -group {cmd detector} -radix decimal /p3_tb/p3_top/uart_module/seq_decode/cmd_detector/i_data
add wave -noupdate -group decoder -group {cmd detector} -radix decimal /p3_tb/p3_top/uart_module/seq_decode/cmd_detector/o_data
add wave -noupdate -group decoder -group length_detector /p3_tb/p3_top/uart_module/seq_decode/lenght_detector/enb
add wave -noupdate -group decoder -group length_detector -radix decimal /p3_tb/p3_top/uart_module/seq_decode/lenght_detector/i_data
add wave -noupdate -group decoder -group length_detector -radix decimal /p3_tb/p3_top/uart_module/seq_decode/lenght_detector/o_data
add wave -noupdate -group decoder -group portLenght_detector /p3_tb/p3_top/uart_module/seq_decode/portLenght_detector/enb
add wave -noupdate -group decoder -group portLenght_detector /p3_tb/p3_top/uart_module/seq_decode/portLenght_detector/i_data
add wave -noupdate -group decoder -group portLenght_detector /p3_tb/p3_top/uart_module/seq_decode/portLenght_detector/o_data
add wave -noupdate -group decoder -group src_detector /p3_tb/p3_top/uart_module/seq_decode/src_detector/enb
add wave -noupdate -group decoder -group src_detector -radix decimal /p3_tb/p3_top/uart_module/seq_decode/src_detector/i_data
add wave -noupdate -group decoder -group src_detector -radix decimal /p3_tb/p3_top/uart_module/seq_decode/src_detector/o_data
add wave -noupdate -group decoder -group dst_detector /p3_tb/p3_top/uart_module/seq_decode/dst_detector/enb
add wave -noupdate -group decoder -group dst_detector -radix decimal /p3_tb/p3_top/uart_module/seq_decode/dst_detector/i_data
add wave -noupdate -group decoder -group dst_detector -radix decimal /p3_tb/p3_top/uart_module/seq_decode/dst_detector/o_data
add wave -noupdate -group decoder -group cmd_vector /p3_tb/p3_top/uart_module/seq_decode/cmd_vector/enb
add wave -noupdate -group decoder -group cmd_vector /p3_tb/p3_top/uart_module/seq_decode/cmd_vector/i_data
add wave -noupdate -group decoder -group cmd_vector /p3_tb/p3_top/uart_module/seq_decode/cmd_vector/o_data
add wave -noupdate -group decoder -group Dn_vector /p3_tb/p3_top/uart_module/seq_decode/Dn_vector/enb
add wave -noupdate -group decoder -group Dn_vector -radix ascii /p3_tb/p3_top/uart_module/seq_decode/Dn_vector/i_data
add wave -noupdate -group decoder -group Dn_vector -radix ascii /p3_tb/p3_top/uart_module/seq_decode/Dn_vector/o_data
add wave -noupdate -divider QSYS
add wave -noupdate -divider QSYS
add wave -noupdate -divider iFIFO
add wave -noupdate /p3_tb/p3_top/data_processing
add wave -noupdate -group ififo0 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[0]/iport/iFIFO_data/push}
add wave -noupdate -group ififo0 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[0]/iport/iFIFO_data/pop}
add wave -noupdate -group ififo0 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[0]/iport/iFIFO_data/restore_pointers}
add wave -noupdate -group ififo0 -group {fifo data} -radix ascii {/p3_tb/p3_top/qsys_module/qsys[0]/iport/iFIFO_data/DataInput}
add wave -noupdate -group ififo0 -group {fifo data} -radix ascii {/p3_tb/p3_top/qsys_module/qsys[0]/iport/iFIFO_data/DataOutput}
add wave -noupdate -group ififo0 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[0]/iport/iFIFO_data/full}
add wave -noupdate -group ififo0 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[0]/iport/iFIFO_data/empty}
add wave -noupdate -group ififo0 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[0]/iport/iFIFO_cmd/push}
add wave -noupdate -group ififo0 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[0]/iport/iFIFO_cmd/pop}
add wave -noupdate -group ififo0 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[0]/iport/iFIFO_cmd/restore_pointers}
add wave -noupdate -group ififo0 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[0]/iport/iFIFO_cmd/DataInput}
add wave -noupdate -group ififo0 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[0]/iport/iFIFO_cmd/DataOutput}
add wave -noupdate -group ififo0 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[0]/iport/iFIFO_cmd/full}
add wave -noupdate -group ififo0 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[0]/iport/iFIFO_cmd/empty}
add wave -noupdate -group ififo0 -radix ascii {/p3_tb/p3_top/qsys_module/qsys[0]/iport/i_data}
add wave -noupdate -group ififo0 -radix binary {/p3_tb/p3_top/qsys_module/qsys[0]/iport/i_cmd}
add wave -noupdate -group ififo0 {/p3_tb/p3_top/qsys_module/qsys[0]/iport/port_number}
add wave -noupdate -group ififo0 {/p3_tb/p3_top/qsys_module/qsys[0]/iport/data_processing}
add wave -noupdate -group ififo0 {/p3_tb/p3_top/qsys_module/qsys[0]/iport/port_enable}
add wave -noupdate -group ififo0 {/p3_tb/p3_top/qsys_module/qsys[0]/iport/execute_req}
add wave -noupdate -group ififo0 {/p3_tb/p3_top/qsys_module/qsys[0]/iport/load}
add wave -noupdate -group ififo0 {/p3_tb/p3_top/qsys_module/qsys[0]/iport/iFIFO_data_push}
add wave -noupdate -group ififo0 {/p3_tb/p3_top/qsys_module/qsys[0]/iport/port_state}
add wave -noupdate -group ififo0 {/p3_tb/p3_top/qsys_module/qsys[0]/iport/port_full}
add wave -noupdate -group ififo0 {/p3_tb/p3_top/qsys_module/qsys[0]/iport/port_empty}
add wave -noupdate -group ififo0 {/p3_tb/p3_top/qsys_module/qsys[0]/iport/o_data}
add wave -noupdate -group ififo0 {/p3_tb/p3_top/qsys_module/qsys[0]/iport/arb_ack}
add wave -noupdate -group ififo0 {/p3_tb/p3_top/qsys_module/qsys[0]/iport/cmd_ready}
add wave -noupdate -group ififo0 {/p3_tb/p3_top/qsys_module/qsys[0]/iport/port_ready}
add wave -noupdate -group ififo0 {/p3_tb/p3_top/qsys_module/qsys[0]/iport/dest_port}
add wave -noupdate -group ififo0 {/p3_tb/p3_top/qsys_module/qsys[0]/iport/iFIFO_data_pop}
add wave -noupdate -group ififo0 {/p3_tb/p3_top/qsys_module/qsys[0]/iport/port_vector}
add wave -noupdate -group ififo1 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[1]/iport/iFIFO_data/push}
add wave -noupdate -group ififo1 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[1]/iport/iFIFO_data/pop}
add wave -noupdate -group ififo1 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[1]/iport/iFIFO_data/restore_pointers}
add wave -noupdate -group ififo1 -group {fifo data} -radix ascii {/p3_tb/p3_top/qsys_module/qsys[1]/iport/iFIFO_data/DataInput}
add wave -noupdate -group ififo1 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[1]/iport/iFIFO_data/DataOutput}
add wave -noupdate -group ififo1 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[1]/iport/iFIFO_data/full}
add wave -noupdate -group ififo1 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[1]/iport/iFIFO_data/empty}
add wave -noupdate -group ififo1 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[1]/iport/iFIFO_cmd/push}
add wave -noupdate -group ififo1 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[1]/iport/iFIFO_cmd/pop}
add wave -noupdate -group ififo1 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[1]/iport/iFIFO_cmd/restore_pointers}
add wave -noupdate -group ififo1 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[1]/iport/iFIFO_cmd/DataInput}
add wave -noupdate -group ififo1 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[1]/iport/iFIFO_cmd/DataOutput}
add wave -noupdate -group ififo1 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[1]/iport/iFIFO_cmd/full}
add wave -noupdate -group ififo1 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[1]/iport/iFIFO_cmd/empty}
add wave -noupdate -group ififo1 {/p3_tb/p3_top/qsys_module/qsys[1]/iport/i_data}
add wave -noupdate -group ififo1 {/p3_tb/p3_top/qsys_module/qsys[1]/iport/i_cmd}
add wave -noupdate -group ififo1 {/p3_tb/p3_top/qsys_module/qsys[1]/iport/port_number}
add wave -noupdate -group ififo1 {/p3_tb/p3_top/qsys_module/qsys[1]/iport/data_processing}
add wave -noupdate -group ififo1 {/p3_tb/p3_top/qsys_module/qsys[1]/iport/port_enable}
add wave -noupdate -group ififo1 {/p3_tb/p3_top/qsys_module/qsys[1]/iport/execute_req}
add wave -noupdate -group ififo1 {/p3_tb/p3_top/qsys_module/qsys[1]/iport/load}
add wave -noupdate -group ififo1 {/p3_tb/p3_top/qsys_module/qsys[1]/iport/iFIFO_data_push}
add wave -noupdate -group ififo1 {/p3_tb/p3_top/qsys_module/qsys[1]/iport/port_state}
add wave -noupdate -group ififo1 {/p3_tb/p3_top/qsys_module/qsys[1]/iport/port_full}
add wave -noupdate -group ififo1 {/p3_tb/p3_top/qsys_module/qsys[1]/iport/port_empty}
add wave -noupdate -group ififo1 {/p3_tb/p3_top/qsys_module/qsys[1]/iport/o_data}
add wave -noupdate -group ififo1 {/p3_tb/p3_top/qsys_module/qsys[1]/iport/arb_ack}
add wave -noupdate -group ififo1 {/p3_tb/p3_top/qsys_module/qsys[1]/iport/cmd_ready}
add wave -noupdate -group ififo1 {/p3_tb/p3_top/qsys_module/qsys[1]/iport/port_ready}
add wave -noupdate -group ififo1 {/p3_tb/p3_top/qsys_module/qsys[1]/iport/dest_port}
add wave -noupdate -group ififo1 {/p3_tb/p3_top/qsys_module/qsys[1]/iport/iFIFO_data_pop}
add wave -noupdate -group ififo1 {/p3_tb/p3_top/qsys_module/qsys[1]/iport/port_vector}
add wave -noupdate -group ififo2 -expand -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[2]/iport/iFIFO_data/push}
add wave -noupdate -group ififo2 -expand -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[2]/iport/iFIFO_data/pop}
add wave -noupdate -group ififo2 -expand -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[2]/iport/iFIFO_data/restore_pointers}
add wave -noupdate -group ififo2 -expand -group {fifo data} -radix ascii {/p3_tb/p3_top/qsys_module/qsys[2]/iport/iFIFO_data/DataInput}
add wave -noupdate -group ififo2 -expand -group {fifo data} -radix ascii {/p3_tb/p3_top/qsys_module/qsys[2]/iport/iFIFO_data/DataOutput}
add wave -noupdate -group ififo2 -expand -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[2]/iport/iFIFO_data/full}
add wave -noupdate -group ififo2 -expand -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[2]/iport/iFIFO_data/empty}
add wave -noupdate -group ififo2 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[2]/iport/iFIFO_cmd/push}
add wave -noupdate -group ififo2 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[2]/iport/iFIFO_cmd/pop}
add wave -noupdate -group ififo2 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[2]/iport/iFIFO_cmd/restore_pointers}
add wave -noupdate -group ififo2 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[2]/iport/iFIFO_cmd/DataInput}
add wave -noupdate -group ififo2 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[2]/iport/iFIFO_cmd/DataOutput}
add wave -noupdate -group ififo2 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[2]/iport/iFIFO_cmd/full}
add wave -noupdate -group ififo2 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[2]/iport/iFIFO_cmd/empty}
add wave -noupdate -group ififo2 -radix ascii {/p3_tb/p3_top/qsys_module/qsys[2]/iport/i_data}
add wave -noupdate -group ififo2 {/p3_tb/p3_top/qsys_module/qsys[2]/iport/i_cmd}
add wave -noupdate -group ififo2 {/p3_tb/p3_top/qsys_module/qsys[2]/iport/port_number}
add wave -noupdate -group ififo2 {/p3_tb/p3_top/qsys_module/qsys[2]/iport/data_processing}
add wave -noupdate -group ififo2 {/p3_tb/p3_top/qsys_module/qsys[2]/iport/port_enable}
add wave -noupdate -group ififo2 {/p3_tb/p3_top/qsys_module/qsys[2]/iport/execute_req}
add wave -noupdate -group ififo2 {/p3_tb/p3_top/qsys_module/qsys[2]/iport/load}
add wave -noupdate -group ififo2 {/p3_tb/p3_top/qsys_module/qsys[2]/iport/iFIFO_data_push}
add wave -noupdate -group ififo2 {/p3_tb/p3_top/qsys_module/qsys[2]/iport/port_state}
add wave -noupdate -group ififo2 {/p3_tb/p3_top/qsys_module/qsys[2]/iport/port_full}
add wave -noupdate -group ififo2 {/p3_tb/p3_top/qsys_module/qsys[2]/iport/port_empty}
add wave -noupdate -group ififo2 {/p3_tb/p3_top/qsys_module/qsys[2]/iport/o_data}
add wave -noupdate -group ififo2 {/p3_tb/p3_top/qsys_module/qsys[2]/iport/arb_ack}
add wave -noupdate -group ififo2 {/p3_tb/p3_top/qsys_module/qsys[2]/iport/cmd_ready}
add wave -noupdate -group ififo2 {/p3_tb/p3_top/qsys_module/qsys[2]/iport/port_ready}
add wave -noupdate -group ififo2 {/p3_tb/p3_top/qsys_module/qsys[2]/iport/dest_port}
add wave -noupdate -group ififo2 {/p3_tb/p3_top/qsys_module/qsys[2]/iport/iFIFO_data_pop}
add wave -noupdate -group ififo2 {/p3_tb/p3_top/qsys_module/qsys[2]/iport/port_vector}
add wave -noupdate -group ififo3 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[3]/iport/iFIFO_data/push}
add wave -noupdate -group ififo3 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[3]/iport/iFIFO_data/pop}
add wave -noupdate -group ififo3 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[3]/iport/iFIFO_data/restore_pointers}
add wave -noupdate -group ififo3 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[3]/iport/iFIFO_data/DataInput}
add wave -noupdate -group ififo3 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[3]/iport/iFIFO_data/DataOutput}
add wave -noupdate -group ififo3 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[3]/iport/iFIFO_data/full}
add wave -noupdate -group ififo3 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[3]/iport/iFIFO_data/empty}
add wave -noupdate -group ififo3 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[3]/iport/iFIFO_cmd/push}
add wave -noupdate -group ififo3 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[3]/iport/iFIFO_cmd/pop}
add wave -noupdate -group ififo3 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[3]/iport/iFIFO_cmd/restore_pointers}
add wave -noupdate -group ififo3 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[3]/iport/iFIFO_cmd/DataInput}
add wave -noupdate -group ififo3 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[3]/iport/iFIFO_cmd/DataOutput}
add wave -noupdate -group ififo3 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[3]/iport/iFIFO_cmd/full}
add wave -noupdate -group ififo3 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[3]/iport/iFIFO_cmd/empty}
add wave -noupdate -group ififo3 {/p3_tb/p3_top/qsys_module/qsys[3]/iport/i_data}
add wave -noupdate -group ififo3 {/p3_tb/p3_top/qsys_module/qsys[3]/iport/i_cmd}
add wave -noupdate -group ififo3 {/p3_tb/p3_top/qsys_module/qsys[3]/iport/port_number}
add wave -noupdate -group ififo3 {/p3_tb/p3_top/qsys_module/qsys[3]/iport/data_processing}
add wave -noupdate -group ififo3 {/p3_tb/p3_top/qsys_module/qsys[3]/iport/port_enable}
add wave -noupdate -group ififo3 {/p3_tb/p3_top/qsys_module/qsys[3]/iport/execute_req}
add wave -noupdate -group ififo3 {/p3_tb/p3_top/qsys_module/qsys[3]/iport/load}
add wave -noupdate -group ififo3 {/p3_tb/p3_top/qsys_module/qsys[3]/iport/iFIFO_data_push}
add wave -noupdate -group ififo3 {/p3_tb/p3_top/qsys_module/qsys[3]/iport/port_state}
add wave -noupdate -group ififo3 {/p3_tb/p3_top/qsys_module/qsys[3]/iport/port_full}
add wave -noupdate -group ififo3 {/p3_tb/p3_top/qsys_module/qsys[3]/iport/port_empty}
add wave -noupdate -group ififo3 -radix ascii {/p3_tb/p3_top/qsys_module/qsys[3]/iport/o_data}
add wave -noupdate -group ififo3 {/p3_tb/p3_top/qsys_module/qsys[3]/iport/arb_ack}
add wave -noupdate -group ififo3 {/p3_tb/p3_top/qsys_module/qsys[3]/iport/cmd_ready}
add wave -noupdate -group ififo3 {/p3_tb/p3_top/qsys_module/qsys[3]/iport/port_ready}
add wave -noupdate -group ififo3 {/p3_tb/p3_top/qsys_module/qsys[3]/iport/dest_port}
add wave -noupdate -group ififo3 {/p3_tb/p3_top/qsys_module/qsys[3]/iport/iFIFO_data_pop}
add wave -noupdate -group ififo3 {/p3_tb/p3_top/qsys_module/qsys[3]/iport/port_vector}
add wave -noupdate -group ififo4 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[4]/iport/iFIFO_data/push}
add wave -noupdate -group ififo4 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[4]/iport/iFIFO_data/pop}
add wave -noupdate -group ififo4 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[4]/iport/iFIFO_data/restore_pointers}
add wave -noupdate -group ififo4 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[4]/iport/iFIFO_data/DataInput}
add wave -noupdate -group ififo4 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[4]/iport/iFIFO_data/DataOutput}
add wave -noupdate -group ififo4 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[4]/iport/iFIFO_data/full}
add wave -noupdate -group ififo4 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[4]/iport/iFIFO_data/empty}
add wave -noupdate -group ififo4 -expand -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[4]/iport/iFIFO_cmd/push}
add wave -noupdate -group ififo4 -expand -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[4]/iport/iFIFO_cmd/pop}
add wave -noupdate -group ififo4 -expand -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[4]/iport/iFIFO_cmd/restore_pointers}
add wave -noupdate -group ififo4 -expand -group {fifo cmd} -radix binary {/p3_tb/p3_top/qsys_module/qsys[4]/iport/iFIFO_cmd/DataInput}
add wave -noupdate -group ififo4 -expand -group {fifo cmd} -radix binary {/p3_tb/p3_top/qsys_module/qsys[4]/iport/iFIFO_cmd/DataOutput}
add wave -noupdate -group ififo4 -expand -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[4]/iport/iFIFO_cmd/full}
add wave -noupdate -group ififo4 -expand -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[4]/iport/iFIFO_cmd/empty}
add wave -noupdate -group ififo4 {/p3_tb/p3_top/qsys_module/qsys[4]/iport/i_data}
add wave -noupdate -group ififo4 {/p3_tb/p3_top/qsys_module/qsys[4]/iport/i_cmd}
add wave -noupdate -group ififo4 {/p3_tb/p3_top/qsys_module/qsys[4]/iport/port_number}
add wave -noupdate -group ififo4 {/p3_tb/p3_top/qsys_module/qsys[4]/iport/data_processing}
add wave -noupdate -group ififo4 {/p3_tb/p3_top/qsys_module/qsys[4]/iport/port_enable}
add wave -noupdate -group ififo4 {/p3_tb/p3_top/qsys_module/qsys[4]/iport/execute_req}
add wave -noupdate -group ififo4 {/p3_tb/p3_top/qsys_module/qsys[4]/iport/load}
add wave -noupdate -group ififo4 {/p3_tb/p3_top/qsys_module/qsys[4]/iport/iFIFO_data_push}
add wave -noupdate -group ififo4 {/p3_tb/p3_top/qsys_module/qsys[4]/iport/port_state}
add wave -noupdate -group ififo4 {/p3_tb/p3_top/qsys_module/qsys[4]/iport/port_full}
add wave -noupdate -group ififo4 {/p3_tb/p3_top/qsys_module/qsys[4]/iport/port_empty}
add wave -noupdate -group ififo4 {/p3_tb/p3_top/qsys_module/qsys[4]/iport/o_data}
add wave -noupdate -group ififo4 {/p3_tb/p3_top/qsys_module/qsys[4]/iport/arb_ack}
add wave -noupdate -group ififo4 {/p3_tb/p3_top/qsys_module/qsys[4]/iport/cmd_ready}
add wave -noupdate -group ififo4 {/p3_tb/p3_top/qsys_module/qsys[4]/iport/port_ready}
add wave -noupdate -group ififo4 -radix decimal {/p3_tb/p3_top/qsys_module/qsys[4]/iport/dest_port}
add wave -noupdate -group ififo4 {/p3_tb/p3_top/qsys_module/qsys[4]/iport/iFIFO_data_pop}
add wave -noupdate -group ififo4 {/p3_tb/p3_top/qsys_module/qsys[4]/iport/port_vector}
add wave -noupdate -group ififo5 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[5]/iport/iFIFO_data/push}
add wave -noupdate -group ififo5 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[5]/iport/iFIFO_data/pop}
add wave -noupdate -group ififo5 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[5]/iport/iFIFO_data/restore_pointers}
add wave -noupdate -group ififo5 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[5]/iport/iFIFO_data/DataInput}
add wave -noupdate -group ififo5 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[5]/iport/iFIFO_data/DataOutput}
add wave -noupdate -group ififo5 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[5]/iport/iFIFO_data/full}
add wave -noupdate -group ififo5 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[5]/iport/iFIFO_data/empty}
add wave -noupdate -group ififo5 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[5]/iport/iFIFO_cmd/push}
add wave -noupdate -group ififo5 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[5]/iport/iFIFO_cmd/pop}
add wave -noupdate -group ififo5 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[5]/iport/iFIFO_cmd/restore_pointers}
add wave -noupdate -group ififo5 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[5]/iport/iFIFO_cmd/DataInput}
add wave -noupdate -group ififo5 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[5]/iport/iFIFO_cmd/DataOutput}
add wave -noupdate -group ififo5 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[5]/iport/iFIFO_cmd/full}
add wave -noupdate -group ififo5 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[5]/iport/iFIFO_cmd/empty}
add wave -noupdate -group ififo5 {/p3_tb/p3_top/qsys_module/qsys[5]/iport/i_data}
add wave -noupdate -group ififo5 {/p3_tb/p3_top/qsys_module/qsys[5]/iport/i_cmd}
add wave -noupdate -group ififo5 {/p3_tb/p3_top/qsys_module/qsys[5]/iport/port_number}
add wave -noupdate -group ififo5 {/p3_tb/p3_top/qsys_module/qsys[5]/iport/data_processing}
add wave -noupdate -group ififo5 {/p3_tb/p3_top/qsys_module/qsys[5]/iport/port_enable}
add wave -noupdate -group ififo5 {/p3_tb/p3_top/qsys_module/qsys[5]/iport/execute_req}
add wave -noupdate -group ififo5 {/p3_tb/p3_top/qsys_module/qsys[5]/iport/load}
add wave -noupdate -group ififo5 {/p3_tb/p3_top/qsys_module/qsys[5]/iport/iFIFO_data_push}
add wave -noupdate -group ififo5 {/p3_tb/p3_top/qsys_module/qsys[5]/iport/port_state}
add wave -noupdate -group ififo5 {/p3_tb/p3_top/qsys_module/qsys[5]/iport/port_full}
add wave -noupdate -group ififo5 {/p3_tb/p3_top/qsys_module/qsys[5]/iport/port_empty}
add wave -noupdate -group ififo5 {/p3_tb/p3_top/qsys_module/qsys[5]/iport/o_data}
add wave -noupdate -group ififo5 {/p3_tb/p3_top/qsys_module/qsys[5]/iport/arb_ack}
add wave -noupdate -group ififo5 {/p3_tb/p3_top/qsys_module/qsys[5]/iport/cmd_ready}
add wave -noupdate -group ififo5 {/p3_tb/p3_top/qsys_module/qsys[5]/iport/port_ready}
add wave -noupdate -group ififo5 {/p3_tb/p3_top/qsys_module/qsys[5]/iport/dest_port}
add wave -noupdate -group ififo5 {/p3_tb/p3_top/qsys_module/qsys[5]/iport/iFIFO_data_pop}
add wave -noupdate -group ififo5 {/p3_tb/p3_top/qsys_module/qsys[5]/iport/port_vector}
add wave -noupdate -group ififo6 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[6]/iport/iFIFO_data/push}
add wave -noupdate -group ififo6 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[6]/iport/iFIFO_data/pop}
add wave -noupdate -group ififo6 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[6]/iport/iFIFO_data/restore_pointers}
add wave -noupdate -group ififo6 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[6]/iport/iFIFO_data/DataInput}
add wave -noupdate -group ififo6 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[6]/iport/iFIFO_data/DataOutput}
add wave -noupdate -group ififo6 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[6]/iport/iFIFO_data/full}
add wave -noupdate -group ififo6 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[6]/iport/iFIFO_data/empty}
add wave -noupdate -group ififo6 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[6]/iport/iFIFO_cmd/push}
add wave -noupdate -group ififo6 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[6]/iport/iFIFO_cmd/pop}
add wave -noupdate -group ififo6 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[6]/iport/iFIFO_cmd/restore_pointers}
add wave -noupdate -group ififo6 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[6]/iport/iFIFO_cmd/DataInput}
add wave -noupdate -group ififo6 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[6]/iport/iFIFO_cmd/DataOutput}
add wave -noupdate -group ififo6 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[6]/iport/iFIFO_cmd/full}
add wave -noupdate -group ififo6 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[6]/iport/iFIFO_cmd/empty}
add wave -noupdate -group ififo6 {/p3_tb/p3_top/qsys_module/qsys[6]/iport/i_data}
add wave -noupdate -group ififo6 {/p3_tb/p3_top/qsys_module/qsys[6]/iport/i_cmd}
add wave -noupdate -group ififo6 {/p3_tb/p3_top/qsys_module/qsys[6]/iport/port_number}
add wave -noupdate -group ififo6 {/p3_tb/p3_top/qsys_module/qsys[6]/iport/data_processing}
add wave -noupdate -group ififo6 {/p3_tb/p3_top/qsys_module/qsys[6]/iport/port_enable}
add wave -noupdate -group ififo6 {/p3_tb/p3_top/qsys_module/qsys[6]/iport/execute_req}
add wave -noupdate -group ififo6 {/p3_tb/p3_top/qsys_module/qsys[6]/iport/load}
add wave -noupdate -group ififo6 {/p3_tb/p3_top/qsys_module/qsys[6]/iport/iFIFO_data_push}
add wave -noupdate -group ififo6 {/p3_tb/p3_top/qsys_module/qsys[6]/iport/port_state}
add wave -noupdate -group ififo6 {/p3_tb/p3_top/qsys_module/qsys[6]/iport/port_full}
add wave -noupdate -group ififo6 {/p3_tb/p3_top/qsys_module/qsys[6]/iport/port_empty}
add wave -noupdate -group ififo6 {/p3_tb/p3_top/qsys_module/qsys[6]/iport/o_data}
add wave -noupdate -group ififo6 {/p3_tb/p3_top/qsys_module/qsys[6]/iport/arb_ack}
add wave -noupdate -group ififo6 {/p3_tb/p3_top/qsys_module/qsys[6]/iport/cmd_ready}
add wave -noupdate -group ififo6 {/p3_tb/p3_top/qsys_module/qsys[6]/iport/port_ready}
add wave -noupdate -group ififo6 {/p3_tb/p3_top/qsys_module/qsys[6]/iport/dest_port}
add wave -noupdate -group ififo6 {/p3_tb/p3_top/qsys_module/qsys[6]/iport/iFIFO_data_pop}
add wave -noupdate -group ififo6 {/p3_tb/p3_top/qsys_module/qsys[6]/iport/port_vector}
add wave -noupdate -group ififo7 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[7]/iport/iFIFO_data/push}
add wave -noupdate -group ififo7 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[7]/iport/iFIFO_data/pop}
add wave -noupdate -group ififo7 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[7]/iport/iFIFO_data/restore_pointers}
add wave -noupdate -group ififo7 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[7]/iport/iFIFO_data/DataInput}
add wave -noupdate -group ififo7 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[7]/iport/iFIFO_data/DataOutput}
add wave -noupdate -group ififo7 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[7]/iport/iFIFO_data/full}
add wave -noupdate -group ififo7 -group {fifo data} {/p3_tb/p3_top/qsys_module/qsys[7]/iport/iFIFO_data/empty}
add wave -noupdate -group ififo7 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[7]/iport/iFIFO_cmd/push}
add wave -noupdate -group ififo7 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[7]/iport/iFIFO_cmd/pop}
add wave -noupdate -group ififo7 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[7]/iport/iFIFO_cmd/restore_pointers}
add wave -noupdate -group ififo7 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[7]/iport/iFIFO_cmd/DataInput}
add wave -noupdate -group ififo7 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[7]/iport/iFIFO_cmd/DataOutput}
add wave -noupdate -group ififo7 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[7]/iport/iFIFO_cmd/full}
add wave -noupdate -group ififo7 -group {fifo cmd} {/p3_tb/p3_top/qsys_module/qsys[7]/iport/iFIFO_cmd/empty}
add wave -noupdate -group ififo7 {/p3_tb/p3_top/qsys_module/qsys[7]/iport/i_data}
add wave -noupdate -group ififo7 {/p3_tb/p3_top/qsys_module/qsys[7]/iport/i_cmd}
add wave -noupdate -group ififo7 {/p3_tb/p3_top/qsys_module/qsys[7]/iport/port_number}
add wave -noupdate -group ififo7 {/p3_tb/p3_top/qsys_module/qsys[7]/iport/data_processing}
add wave -noupdate -group ififo7 {/p3_tb/p3_top/qsys_module/qsys[7]/iport/port_enable}
add wave -noupdate -group ififo7 {/p3_tb/p3_top/qsys_module/qsys[7]/iport/execute_req}
add wave -noupdate -group ififo7 {/p3_tb/p3_top/qsys_module/qsys[7]/iport/load}
add wave -noupdate -group ififo7 {/p3_tb/p3_top/qsys_module/qsys[7]/iport/iFIFO_data_push}
add wave -noupdate -group ififo7 {/p3_tb/p3_top/qsys_module/qsys[7]/iport/port_state}
add wave -noupdate -group ififo7 {/p3_tb/p3_top/qsys_module/qsys[7]/iport/port_full}
add wave -noupdate -group ififo7 {/p3_tb/p3_top/qsys_module/qsys[7]/iport/port_empty}
add wave -noupdate -group ififo7 {/p3_tb/p3_top/qsys_module/qsys[7]/iport/o_data}
add wave -noupdate -group ififo7 {/p3_tb/p3_top/qsys_module/qsys[7]/iport/arb_ack}
add wave -noupdate -group ififo7 {/p3_tb/p3_top/qsys_module/qsys[7]/iport/cmd_ready}
add wave -noupdate -group ififo7 {/p3_tb/p3_top/qsys_module/qsys[7]/iport/port_ready}
add wave -noupdate -group ififo7 {/p3_tb/p3_top/qsys_module/qsys[7]/iport/dest_port}
add wave -noupdate -group ififo7 {/p3_tb/p3_top/qsys_module/qsys[7]/iport/iFIFO_data_pop}
add wave -noupdate -group ififo7 {/p3_tb/p3_top/qsys_module/qsys[7]/iport/port_vector}
add wave -noupdate -divider ARBEITER
add wave -noupdate -group arb0 {/p3_tb/p3_top/qsys_module/qsys[0]/arbeiter/ack}
add wave -noupdate -group arb0 {/p3_tb/p3_top/qsys_module/qsys[0]/arbeiter/request_vector}
add wave -noupdate -group arb0 {/p3_tb/p3_top/qsys_module/qsys[0]/arbeiter/valid}
add wave -noupdate -group arb0 {/p3_tb/p3_top/qsys_module/qsys[0]/arbeiter/grant}
add wave -noupdate -group arb1 {/p3_tb/p3_top/qsys_module/qsys[1]/arbeiter/ack}
add wave -noupdate -group arb1 {/p3_tb/p3_top/qsys_module/qsys[1]/arbeiter/request_vector}
add wave -noupdate -group arb1 {/p3_tb/p3_top/qsys_module/qsys[1]/arbeiter/valid}
add wave -noupdate -group arb1 {/p3_tb/p3_top/qsys_module/qsys[1]/arbeiter/grant}
add wave -noupdate -group arb2 {/p3_tb/p3_top/qsys_module/qsys[2]/arbeiter/ack}
add wave -noupdate -group arb2 {/p3_tb/p3_top/qsys_module/qsys[2]/arbeiter/request_vector}
add wave -noupdate -group arb2 {/p3_tb/p3_top/qsys_module/qsys[2]/arbeiter/valid}
add wave -noupdate -group arb2 {/p3_tb/p3_top/qsys_module/qsys[2]/arbeiter/grant}
add wave -noupdate -group arb3 {/p3_tb/p3_top/qsys_module/qsys[3]/arbeiter/ack}
add wave -noupdate -group arb3 {/p3_tb/p3_top/qsys_module/qsys[3]/arbeiter/request_vector}
add wave -noupdate -group arb3 {/p3_tb/p3_top/qsys_module/qsys[3]/arbeiter/valid}
add wave -noupdate -group arb3 {/p3_tb/p3_top/qsys_module/qsys[3]/arbeiter/grant}
add wave -noupdate -group arb4 {/p3_tb/p3_top/qsys_module/qsys[4]/arbeiter/ack}
add wave -noupdate -group arb4 {/p3_tb/p3_top/qsys_module/qsys[4]/arbeiter/request_vector}
add wave -noupdate -group arb4 {/p3_tb/p3_top/qsys_module/qsys[4]/arbeiter/valid}
add wave -noupdate -group arb4 {/p3_tb/p3_top/qsys_module/qsys[4]/arbeiter/grant}
add wave -noupdate -group arb5 {/p3_tb/p3_top/qsys_module/qsys[5]/arbeiter/ack}
add wave -noupdate -group arb5 {/p3_tb/p3_top/qsys_module/qsys[5]/arbeiter/request_vector}
add wave -noupdate -group arb5 {/p3_tb/p3_top/qsys_module/qsys[5]/arbeiter/valid}
add wave -noupdate -group arb5 {/p3_tb/p3_top/qsys_module/qsys[5]/arbeiter/grant}
add wave -noupdate -group arb6 {/p3_tb/p3_top/qsys_module/qsys[6]/arbeiter/ack}
add wave -noupdate -group arb6 {/p3_tb/p3_top/qsys_module/qsys[6]/arbeiter/request_vector}
add wave -noupdate -group arb6 {/p3_tb/p3_top/qsys_module/qsys[6]/arbeiter/valid}
add wave -noupdate -group arb6 {/p3_tb/p3_top/qsys_module/qsys[6]/arbeiter/grant}
add wave -noupdate -group arb7 {/p3_tb/p3_top/qsys_module/qsys[7]/arbeiter/ack}
add wave -noupdate -group arb7 {/p3_tb/p3_top/qsys_module/qsys[7]/arbeiter/request_vector}
add wave -noupdate -group arb7 {/p3_tb/p3_top/qsys_module/qsys[7]/arbeiter/valid}
add wave -noupdate -group arb7 {/p3_tb/p3_top/qsys_module/qsys[7]/arbeiter/grant}
add wave -noupdate -divider {DEMUX VECTOR}
add wave -noupdate -group demux_req_vector0 {/p3_tb/p3_top/qsys_module/qsys[0]/req_vector_iport/i_data}
add wave -noupdate -group demux_req_vector0 {/p3_tb/p3_top/qsys_module/qsys[0]/req_vector_iport/i_sel}
add wave -noupdate -group demux_req_vector0 {/p3_tb/p3_top/qsys_module/qsys[0]/req_vector_iport/o_data_a}
add wave -noupdate -group demux_req_vector0 {/p3_tb/p3_top/qsys_module/qsys[0]/req_vector_iport/o_data_b}
add wave -noupdate -group demux_req_vector0 {/p3_tb/p3_top/qsys_module/qsys[0]/req_vector_iport/o_data_c}
add wave -noupdate -group demux_req_vector0 {/p3_tb/p3_top/qsys_module/qsys[0]/req_vector_iport/o_data_d}
add wave -noupdate -group demux_req_vector0 {/p3_tb/p3_top/qsys_module/qsys[0]/req_vector_iport/o_data_e}
add wave -noupdate -group demux_req_vector0 {/p3_tb/p3_top/qsys_module/qsys[0]/req_vector_iport/o_data_f}
add wave -noupdate -group demux_req_vector0 {/p3_tb/p3_top/qsys_module/qsys[0]/req_vector_iport/o_data_g}
add wave -noupdate -group demux_req_vector0 {/p3_tb/p3_top/qsys_module/qsys[0]/req_vector_iport/o_data_h}
add wave -noupdate -group demux_req_vector1 {/p3_tb/p3_top/qsys_module/qsys[1]/req_vector_iport/i_data}
add wave -noupdate -group demux_req_vector1 {/p3_tb/p3_top/qsys_module/qsys[1]/req_vector_iport/i_sel}
add wave -noupdate -group demux_req_vector1 {/p3_tb/p3_top/qsys_module/qsys[1]/req_vector_iport/o_data_a}
add wave -noupdate -group demux_req_vector1 {/p3_tb/p3_top/qsys_module/qsys[1]/req_vector_iport/o_data_b}
add wave -noupdate -group demux_req_vector1 {/p3_tb/p3_top/qsys_module/qsys[1]/req_vector_iport/o_data_c}
add wave -noupdate -group demux_req_vector1 {/p3_tb/p3_top/qsys_module/qsys[1]/req_vector_iport/o_data_d}
add wave -noupdate -group demux_req_vector1 {/p3_tb/p3_top/qsys_module/qsys[1]/req_vector_iport/o_data_e}
add wave -noupdate -group demux_req_vector1 {/p3_tb/p3_top/qsys_module/qsys[1]/req_vector_iport/o_data_f}
add wave -noupdate -group demux_req_vector1 {/p3_tb/p3_top/qsys_module/qsys[1]/req_vector_iport/o_data_g}
add wave -noupdate -group demux_req_vector1 {/p3_tb/p3_top/qsys_module/qsys[1]/req_vector_iport/o_data_h}
add wave -noupdate -group demux_req_vector2 {/p3_tb/p3_top/qsys_module/qsys[2]/req_vector_iport/i_data}
add wave -noupdate -group demux_req_vector2 {/p3_tb/p3_top/qsys_module/qsys[2]/req_vector_iport/i_sel}
add wave -noupdate -group demux_req_vector2 {/p3_tb/p3_top/qsys_module/qsys[2]/req_vector_iport/o_data_a}
add wave -noupdate -group demux_req_vector2 {/p3_tb/p3_top/qsys_module/qsys[2]/req_vector_iport/o_data_b}
add wave -noupdate -group demux_req_vector2 {/p3_tb/p3_top/qsys_module/qsys[2]/req_vector_iport/o_data_c}
add wave -noupdate -group demux_req_vector2 {/p3_tb/p3_top/qsys_module/qsys[2]/req_vector_iport/o_data_d}
add wave -noupdate -group demux_req_vector2 {/p3_tb/p3_top/qsys_module/qsys[2]/req_vector_iport/o_data_e}
add wave -noupdate -group demux_req_vector2 {/p3_tb/p3_top/qsys_module/qsys[2]/req_vector_iport/o_data_f}
add wave -noupdate -group demux_req_vector2 {/p3_tb/p3_top/qsys_module/qsys[2]/req_vector_iport/o_data_g}
add wave -noupdate -group demux_req_vector2 {/p3_tb/p3_top/qsys_module/qsys[2]/req_vector_iport/o_data_h}
add wave -noupdate -group demux_req_vector3 {/p3_tb/p3_top/qsys_module/qsys[3]/req_vector_iport/i_data}
add wave -noupdate -group demux_req_vector3 {/p3_tb/p3_top/qsys_module/qsys[3]/req_vector_iport/i_sel}
add wave -noupdate -group demux_req_vector3 {/p3_tb/p3_top/qsys_module/qsys[3]/req_vector_iport/o_data_a}
add wave -noupdate -group demux_req_vector3 {/p3_tb/p3_top/qsys_module/qsys[3]/req_vector_iport/o_data_b}
add wave -noupdate -group demux_req_vector3 {/p3_tb/p3_top/qsys_module/qsys[3]/req_vector_iport/o_data_c}
add wave -noupdate -group demux_req_vector3 {/p3_tb/p3_top/qsys_module/qsys[3]/req_vector_iport/o_data_d}
add wave -noupdate -group demux_req_vector3 {/p3_tb/p3_top/qsys_module/qsys[3]/req_vector_iport/o_data_e}
add wave -noupdate -group demux_req_vector3 {/p3_tb/p3_top/qsys_module/qsys[3]/req_vector_iport/o_data_f}
add wave -noupdate -group demux_req_vector3 {/p3_tb/p3_top/qsys_module/qsys[3]/req_vector_iport/o_data_g}
add wave -noupdate -group demux_req_vector3 {/p3_tb/p3_top/qsys_module/qsys[3]/req_vector_iport/o_data_h}
add wave -noupdate -group demux_req_vector4 {/p3_tb/p3_top/qsys_module/qsys[4]/req_vector_iport/i_data}
add wave -noupdate -group demux_req_vector4 {/p3_tb/p3_top/qsys_module/qsys[4]/req_vector_iport/i_sel}
add wave -noupdate -group demux_req_vector4 {/p3_tb/p3_top/qsys_module/qsys[4]/req_vector_iport/o_data_a}
add wave -noupdate -group demux_req_vector4 {/p3_tb/p3_top/qsys_module/qsys[4]/req_vector_iport/o_data_b}
add wave -noupdate -group demux_req_vector4 {/p3_tb/p3_top/qsys_module/qsys[4]/req_vector_iport/o_data_c}
add wave -noupdate -group demux_req_vector4 {/p3_tb/p3_top/qsys_module/qsys[4]/req_vector_iport/o_data_d}
add wave -noupdate -group demux_req_vector4 {/p3_tb/p3_top/qsys_module/qsys[4]/req_vector_iport/o_data_e}
add wave -noupdate -group demux_req_vector4 {/p3_tb/p3_top/qsys_module/qsys[4]/req_vector_iport/o_data_f}
add wave -noupdate -group demux_req_vector4 {/p3_tb/p3_top/qsys_module/qsys[4]/req_vector_iport/o_data_g}
add wave -noupdate -group demux_req_vector4 {/p3_tb/p3_top/qsys_module/qsys[4]/req_vector_iport/o_data_h}
add wave -noupdate -group demux_req_vector5 {/p3_tb/p3_top/qsys_module/qsys[5]/req_vector_iport/i_data}
add wave -noupdate -group demux_req_vector5 {/p3_tb/p3_top/qsys_module/qsys[5]/req_vector_iport/i_sel}
add wave -noupdate -group demux_req_vector5 {/p3_tb/p3_top/qsys_module/qsys[5]/req_vector_iport/o_data_a}
add wave -noupdate -group demux_req_vector5 {/p3_tb/p3_top/qsys_module/qsys[5]/req_vector_iport/o_data_b}
add wave -noupdate -group demux_req_vector5 {/p3_tb/p3_top/qsys_module/qsys[5]/req_vector_iport/o_data_c}
add wave -noupdate -group demux_req_vector5 {/p3_tb/p3_top/qsys_module/qsys[5]/req_vector_iport/o_data_d}
add wave -noupdate -group demux_req_vector5 {/p3_tb/p3_top/qsys_module/qsys[5]/req_vector_iport/o_data_e}
add wave -noupdate -group demux_req_vector5 {/p3_tb/p3_top/qsys_module/qsys[5]/req_vector_iport/o_data_f}
add wave -noupdate -group demux_req_vector5 {/p3_tb/p3_top/qsys_module/qsys[5]/req_vector_iport/o_data_g}
add wave -noupdate -group demux_req_vector5 {/p3_tb/p3_top/qsys_module/qsys[5]/req_vector_iport/o_data_h}
add wave -noupdate -group demux_req_vector6 {/p3_tb/p3_top/qsys_module/qsys[6]/req_vector_iport/i_data}
add wave -noupdate -group demux_req_vector6 {/p3_tb/p3_top/qsys_module/qsys[6]/req_vector_iport/i_sel}
add wave -noupdate -group demux_req_vector6 {/p3_tb/p3_top/qsys_module/qsys[6]/req_vector_iport/o_data_a}
add wave -noupdate -group demux_req_vector6 {/p3_tb/p3_top/qsys_module/qsys[6]/req_vector_iport/o_data_b}
add wave -noupdate -group demux_req_vector6 {/p3_tb/p3_top/qsys_module/qsys[6]/req_vector_iport/o_data_c}
add wave -noupdate -group demux_req_vector6 {/p3_tb/p3_top/qsys_module/qsys[6]/req_vector_iport/o_data_d}
add wave -noupdate -group demux_req_vector6 {/p3_tb/p3_top/qsys_module/qsys[6]/req_vector_iport/o_data_e}
add wave -noupdate -group demux_req_vector6 {/p3_tb/p3_top/qsys_module/qsys[6]/req_vector_iport/o_data_f}
add wave -noupdate -group demux_req_vector6 {/p3_tb/p3_top/qsys_module/qsys[6]/req_vector_iport/o_data_g}
add wave -noupdate -group demux_req_vector6 {/p3_tb/p3_top/qsys_module/qsys[6]/req_vector_iport/o_data_h}
add wave -noupdate -group demux_req_vector7 {/p3_tb/p3_top/qsys_module/qsys[7]/req_vector_iport/i_data}
add wave -noupdate -group demux_req_vector7 {/p3_tb/p3_top/qsys_module/qsys[7]/req_vector_iport/i_sel}
add wave -noupdate -group demux_req_vector7 {/p3_tb/p3_top/qsys_module/qsys[7]/req_vector_iport/o_data_a}
add wave -noupdate -group demux_req_vector7 {/p3_tb/p3_top/qsys_module/qsys[7]/req_vector_iport/o_data_b}
add wave -noupdate -group demux_req_vector7 {/p3_tb/p3_top/qsys_module/qsys[7]/req_vector_iport/o_data_c}
add wave -noupdate -group demux_req_vector7 {/p3_tb/p3_top/qsys_module/qsys[7]/req_vector_iport/o_data_d}
add wave -noupdate -group demux_req_vector7 {/p3_tb/p3_top/qsys_module/qsys[7]/req_vector_iport/o_data_e}
add wave -noupdate -group demux_req_vector7 {/p3_tb/p3_top/qsys_module/qsys[7]/req_vector_iport/o_data_f}
add wave -noupdate -group demux_req_vector7 {/p3_tb/p3_top/qsys_module/qsys[7]/req_vector_iport/o_data_g}
add wave -noupdate -group demux_req_vector7 {/p3_tb/p3_top/qsys_module/qsys[7]/req_vector_iport/o_data_h}
add wave -noupdate -divider {OR GRANT}
add wave -noupdate -group or_grant0 {/p3_tb/p3_top/qsys_module/qsys[0]/or_grant_8_1/i_a}
add wave -noupdate -group or_grant0 {/p3_tb/p3_top/qsys_module/qsys[0]/or_grant_8_1/i_b}
add wave -noupdate -group or_grant0 {/p3_tb/p3_top/qsys_module/qsys[0]/or_grant_8_1/i_c}
add wave -noupdate -group or_grant0 {/p3_tb/p3_top/qsys_module/qsys[0]/or_grant_8_1/i_d}
add wave -noupdate -group or_grant0 {/p3_tb/p3_top/qsys_module/qsys[0]/or_grant_8_1/i_e}
add wave -noupdate -group or_grant0 {/p3_tb/p3_top/qsys_module/qsys[0]/or_grant_8_1/i_f}
add wave -noupdate -group or_grant0 {/p3_tb/p3_top/qsys_module/qsys[0]/or_grant_8_1/i_g}
add wave -noupdate -group or_grant0 {/p3_tb/p3_top/qsys_module/qsys[0]/or_grant_8_1/i_h}
add wave -noupdate -group or_grant0 {/p3_tb/p3_top/qsys_module/qsys[0]/or_grant_8_1/o_sltd}
add wave -noupdate -group or_grant1 {/p3_tb/p3_top/qsys_module/qsys[1]/or_grant_8_1/i_a}
add wave -noupdate -group or_grant1 {/p3_tb/p3_top/qsys_module/qsys[1]/or_grant_8_1/i_b}
add wave -noupdate -group or_grant1 {/p3_tb/p3_top/qsys_module/qsys[1]/or_grant_8_1/i_c}
add wave -noupdate -group or_grant1 {/p3_tb/p3_top/qsys_module/qsys[1]/or_grant_8_1/i_d}
add wave -noupdate -group or_grant1 {/p3_tb/p3_top/qsys_module/qsys[1]/or_grant_8_1/i_e}
add wave -noupdate -group or_grant1 {/p3_tb/p3_top/qsys_module/qsys[1]/or_grant_8_1/i_f}
add wave -noupdate -group or_grant1 {/p3_tb/p3_top/qsys_module/qsys[1]/or_grant_8_1/i_g}
add wave -noupdate -group or_grant1 {/p3_tb/p3_top/qsys_module/qsys[1]/or_grant_8_1/i_h}
add wave -noupdate -group or_grant1 {/p3_tb/p3_top/qsys_module/qsys[1]/or_grant_8_1/o_sltd}
add wave -noupdate -group or_grant2 {/p3_tb/p3_top/qsys_module/qsys[2]/or_grant_8_1/i_a}
add wave -noupdate -group or_grant2 {/p3_tb/p3_top/qsys_module/qsys[2]/or_grant_8_1/i_b}
add wave -noupdate -group or_grant2 {/p3_tb/p3_top/qsys_module/qsys[2]/or_grant_8_1/i_c}
add wave -noupdate -group or_grant2 {/p3_tb/p3_top/qsys_module/qsys[2]/or_grant_8_1/i_d}
add wave -noupdate -group or_grant2 {/p3_tb/p3_top/qsys_module/qsys[2]/or_grant_8_1/i_e}
add wave -noupdate -group or_grant2 {/p3_tb/p3_top/qsys_module/qsys[2]/or_grant_8_1/i_f}
add wave -noupdate -group or_grant2 {/p3_tb/p3_top/qsys_module/qsys[2]/or_grant_8_1/i_g}
add wave -noupdate -group or_grant2 {/p3_tb/p3_top/qsys_module/qsys[2]/or_grant_8_1/i_h}
add wave -noupdate -group or_grant2 {/p3_tb/p3_top/qsys_module/qsys[2]/or_grant_8_1/o_sltd}
add wave -noupdate -group or_grant3 {/p3_tb/p3_top/qsys_module/qsys[3]/or_grant_8_1/i_a}
add wave -noupdate -group or_grant3 {/p3_tb/p3_top/qsys_module/qsys[3]/or_grant_8_1/i_b}
add wave -noupdate -group or_grant3 {/p3_tb/p3_top/qsys_module/qsys[3]/or_grant_8_1/i_c}
add wave -noupdate -group or_grant3 {/p3_tb/p3_top/qsys_module/qsys[3]/or_grant_8_1/i_d}
add wave -noupdate -group or_grant3 {/p3_tb/p3_top/qsys_module/qsys[3]/or_grant_8_1/i_e}
add wave -noupdate -group or_grant3 {/p3_tb/p3_top/qsys_module/qsys[3]/or_grant_8_1/i_f}
add wave -noupdate -group or_grant3 {/p3_tb/p3_top/qsys_module/qsys[3]/or_grant_8_1/i_g}
add wave -noupdate -group or_grant3 {/p3_tb/p3_top/qsys_module/qsys[3]/or_grant_8_1/i_h}
add wave -noupdate -group or_grant3 {/p3_tb/p3_top/qsys_module/qsys[3]/or_grant_8_1/o_sltd}
add wave -noupdate -group or_grant4 {/p3_tb/p3_top/qsys_module/qsys[4]/or_grant_8_1/i_a}
add wave -noupdate -group or_grant4 {/p3_tb/p3_top/qsys_module/qsys[4]/or_grant_8_1/i_b}
add wave -noupdate -group or_grant4 {/p3_tb/p3_top/qsys_module/qsys[4]/or_grant_8_1/i_c}
add wave -noupdate -group or_grant4 {/p3_tb/p3_top/qsys_module/qsys[4]/or_grant_8_1/i_d}
add wave -noupdate -group or_grant4 {/p3_tb/p3_top/qsys_module/qsys[4]/or_grant_8_1/i_e}
add wave -noupdate -group or_grant4 {/p3_tb/p3_top/qsys_module/qsys[4]/or_grant_8_1/i_f}
add wave -noupdate -group or_grant4 {/p3_tb/p3_top/qsys_module/qsys[4]/or_grant_8_1/i_g}
add wave -noupdate -group or_grant4 {/p3_tb/p3_top/qsys_module/qsys[4]/or_grant_8_1/i_h}
add wave -noupdate -group or_grant4 {/p3_tb/p3_top/qsys_module/qsys[4]/or_grant_8_1/o_sltd}
add wave -noupdate -group or_grant5 {/p3_tb/p3_top/qsys_module/qsys[5]/or_grant_8_1/i_a}
add wave -noupdate -group or_grant5 {/p3_tb/p3_top/qsys_module/qsys[5]/or_grant_8_1/i_b}
add wave -noupdate -group or_grant5 {/p3_tb/p3_top/qsys_module/qsys[5]/or_grant_8_1/i_c}
add wave -noupdate -group or_grant5 {/p3_tb/p3_top/qsys_module/qsys[5]/or_grant_8_1/i_d}
add wave -noupdate -group or_grant5 {/p3_tb/p3_top/qsys_module/qsys[5]/or_grant_8_1/i_e}
add wave -noupdate -group or_grant5 {/p3_tb/p3_top/qsys_module/qsys[5]/or_grant_8_1/i_f}
add wave -noupdate -group or_grant5 {/p3_tb/p3_top/qsys_module/qsys[5]/or_grant_8_1/i_g}
add wave -noupdate -group or_grant5 {/p3_tb/p3_top/qsys_module/qsys[5]/or_grant_8_1/i_h}
add wave -noupdate -group or_grant5 {/p3_tb/p3_top/qsys_module/qsys[5]/or_grant_8_1/o_sltd}
add wave -noupdate -group or_grant6 {/p3_tb/p3_top/qsys_module/qsys[6]/or_grant_8_1/i_a}
add wave -noupdate -group or_grant6 {/p3_tb/p3_top/qsys_module/qsys[6]/or_grant_8_1/i_b}
add wave -noupdate -group or_grant6 {/p3_tb/p3_top/qsys_module/qsys[6]/or_grant_8_1/i_c}
add wave -noupdate -group or_grant6 {/p3_tb/p3_top/qsys_module/qsys[6]/or_grant_8_1/i_d}
add wave -noupdate -group or_grant6 {/p3_tb/p3_top/qsys_module/qsys[6]/or_grant_8_1/i_e}
add wave -noupdate -group or_grant6 {/p3_tb/p3_top/qsys_module/qsys[6]/or_grant_8_1/i_f}
add wave -noupdate -group or_grant6 {/p3_tb/p3_top/qsys_module/qsys[6]/or_grant_8_1/i_g}
add wave -noupdate -group or_grant6 {/p3_tb/p3_top/qsys_module/qsys[6]/or_grant_8_1/i_h}
add wave -noupdate -group or_grant6 {/p3_tb/p3_top/qsys_module/qsys[6]/or_grant_8_1/o_sltd}
add wave -noupdate -group or_grant7 {/p3_tb/p3_top/qsys_module/qsys[7]/or_grant_8_1/i_a}
add wave -noupdate -group or_grant7 {/p3_tb/p3_top/qsys_module/qsys[7]/or_grant_8_1/i_b}
add wave -noupdate -group or_grant7 {/p3_tb/p3_top/qsys_module/qsys[7]/or_grant_8_1/i_c}
add wave -noupdate -group or_grant7 {/p3_tb/p3_top/qsys_module/qsys[7]/or_grant_8_1/i_d}
add wave -noupdate -group or_grant7 {/p3_tb/p3_top/qsys_module/qsys[7]/or_grant_8_1/i_e}
add wave -noupdate -group or_grant7 {/p3_tb/p3_top/qsys_module/qsys[7]/or_grant_8_1/i_f}
add wave -noupdate -group or_grant7 {/p3_tb/p3_top/qsys_module/qsys[7]/or_grant_8_1/i_g}
add wave -noupdate -group or_grant7 {/p3_tb/p3_top/qsys_module/qsys[7]/or_grant_8_1/i_h}
add wave -noupdate -group or_grant7 {/p3_tb/p3_top/qsys_module/qsys[7]/or_grant_8_1/o_sltd}
add wave -noupdate -divider {OR CONCAT}
add wave -noupdate -group or_concat0 {/p3_tb/p3_top/qsys_module/qsys[0]/or_req_8_1/i_a}
add wave -noupdate -group or_concat0 {/p3_tb/p3_top/qsys_module/qsys[0]/or_req_8_1/i_b}
add wave -noupdate -group or_concat0 {/p3_tb/p3_top/qsys_module/qsys[0]/or_req_8_1/i_c}
add wave -noupdate -group or_concat0 {/p3_tb/p3_top/qsys_module/qsys[0]/or_req_8_1/i_d}
add wave -noupdate -group or_concat0 {/p3_tb/p3_top/qsys_module/qsys[0]/or_req_8_1/i_e}
add wave -noupdate -group or_concat0 {/p3_tb/p3_top/qsys_module/qsys[0]/or_req_8_1/i_f}
add wave -noupdate -group or_concat0 {/p3_tb/p3_top/qsys_module/qsys[0]/or_req_8_1/i_g}
add wave -noupdate -group or_concat0 {/p3_tb/p3_top/qsys_module/qsys[0]/or_req_8_1/i_h}
add wave -noupdate -group or_concat0 {/p3_tb/p3_top/qsys_module/qsys[0]/or_req_8_1/o_sltd}
add wave -noupdate -group or_concat1 {/p3_tb/p3_top/qsys_module/qsys[1]/or_req_8_1/i_a}
add wave -noupdate -group or_concat1 {/p3_tb/p3_top/qsys_module/qsys[1]/or_req_8_1/i_b}
add wave -noupdate -group or_concat1 {/p3_tb/p3_top/qsys_module/qsys[1]/or_req_8_1/i_c}
add wave -noupdate -group or_concat1 {/p3_tb/p3_top/qsys_module/qsys[1]/or_req_8_1/i_d}
add wave -noupdate -group or_concat1 {/p3_tb/p3_top/qsys_module/qsys[1]/or_req_8_1/i_e}
add wave -noupdate -group or_concat1 {/p3_tb/p3_top/qsys_module/qsys[1]/or_req_8_1/i_f}
add wave -noupdate -group or_concat1 {/p3_tb/p3_top/qsys_module/qsys[1]/or_req_8_1/i_g}
add wave -noupdate -group or_concat1 {/p3_tb/p3_top/qsys_module/qsys[1]/or_req_8_1/i_h}
add wave -noupdate -group or_concat1 {/p3_tb/p3_top/qsys_module/qsys[1]/or_req_8_1/o_sltd}
add wave -noupdate -group or_concat2 {/p3_tb/p3_top/qsys_module/qsys[2]/or_req_8_1/i_a}
add wave -noupdate -group or_concat2 {/p3_tb/p3_top/qsys_module/qsys[2]/or_req_8_1/i_b}
add wave -noupdate -group or_concat2 {/p3_tb/p3_top/qsys_module/qsys[2]/or_req_8_1/i_c}
add wave -noupdate -group or_concat2 {/p3_tb/p3_top/qsys_module/qsys[2]/or_req_8_1/i_d}
add wave -noupdate -group or_concat2 {/p3_tb/p3_top/qsys_module/qsys[2]/or_req_8_1/i_e}
add wave -noupdate -group or_concat2 {/p3_tb/p3_top/qsys_module/qsys[2]/or_req_8_1/i_f}
add wave -noupdate -group or_concat2 {/p3_tb/p3_top/qsys_module/qsys[2]/or_req_8_1/i_g}
add wave -noupdate -group or_concat2 {/p3_tb/p3_top/qsys_module/qsys[2]/or_req_8_1/i_h}
add wave -noupdate -group or_concat2 {/p3_tb/p3_top/qsys_module/qsys[2]/or_req_8_1/o_sltd}
add wave -noupdate -group or_concat3 {/p3_tb/p3_top/qsys_module/qsys[3]/or_req_8_1/i_a}
add wave -noupdate -group or_concat3 {/p3_tb/p3_top/qsys_module/qsys[3]/or_req_8_1/i_b}
add wave -noupdate -group or_concat3 {/p3_tb/p3_top/qsys_module/qsys[3]/or_req_8_1/i_c}
add wave -noupdate -group or_concat3 {/p3_tb/p3_top/qsys_module/qsys[3]/or_req_8_1/i_d}
add wave -noupdate -group or_concat3 {/p3_tb/p3_top/qsys_module/qsys[3]/or_req_8_1/i_e}
add wave -noupdate -group or_concat3 {/p3_tb/p3_top/qsys_module/qsys[3]/or_req_8_1/i_f}
add wave -noupdate -group or_concat3 {/p3_tb/p3_top/qsys_module/qsys[3]/or_req_8_1/i_g}
add wave -noupdate -group or_concat3 {/p3_tb/p3_top/qsys_module/qsys[3]/or_req_8_1/i_h}
add wave -noupdate -group or_concat3 {/p3_tb/p3_top/qsys_module/qsys[3]/or_req_8_1/o_sltd}
add wave -noupdate -group or_concat4 {/p3_tb/p3_top/qsys_module/qsys[4]/or_req_8_1/i_a}
add wave -noupdate -group or_concat4 {/p3_tb/p3_top/qsys_module/qsys[4]/or_req_8_1/i_b}
add wave -noupdate -group or_concat4 {/p3_tb/p3_top/qsys_module/qsys[4]/or_req_8_1/i_c}
add wave -noupdate -group or_concat4 {/p3_tb/p3_top/qsys_module/qsys[4]/or_req_8_1/i_d}
add wave -noupdate -group or_concat4 {/p3_tb/p3_top/qsys_module/qsys[4]/or_req_8_1/i_e}
add wave -noupdate -group or_concat4 {/p3_tb/p3_top/qsys_module/qsys[4]/or_req_8_1/i_f}
add wave -noupdate -group or_concat4 {/p3_tb/p3_top/qsys_module/qsys[4]/or_req_8_1/i_g}
add wave -noupdate -group or_concat4 {/p3_tb/p3_top/qsys_module/qsys[4]/or_req_8_1/i_h}
add wave -noupdate -group or_concat4 {/p3_tb/p3_top/qsys_module/qsys[4]/or_req_8_1/o_sltd}
add wave -noupdate -group or_concat5 {/p3_tb/p3_top/qsys_module/qsys[5]/or_req_8_1/i_a}
add wave -noupdate -group or_concat5 {/p3_tb/p3_top/qsys_module/qsys[5]/or_req_8_1/i_b}
add wave -noupdate -group or_concat5 {/p3_tb/p3_top/qsys_module/qsys[5]/or_req_8_1/i_c}
add wave -noupdate -group or_concat5 {/p3_tb/p3_top/qsys_module/qsys[5]/or_req_8_1/i_d}
add wave -noupdate -group or_concat5 {/p3_tb/p3_top/qsys_module/qsys[5]/or_req_8_1/i_e}
add wave -noupdate -group or_concat5 {/p3_tb/p3_top/qsys_module/qsys[5]/or_req_8_1/i_f}
add wave -noupdate -group or_concat5 {/p3_tb/p3_top/qsys_module/qsys[5]/or_req_8_1/i_g}
add wave -noupdate -group or_concat5 {/p3_tb/p3_top/qsys_module/qsys[5]/or_req_8_1/i_h}
add wave -noupdate -group or_concat5 {/p3_tb/p3_top/qsys_module/qsys[5]/or_req_8_1/o_sltd}
add wave -noupdate -group or_concat6 {/p3_tb/p3_top/qsys_module/qsys[6]/or_req_8_1/i_a}
add wave -noupdate -group or_concat6 {/p3_tb/p3_top/qsys_module/qsys[6]/or_req_8_1/i_b}
add wave -noupdate -group or_concat6 {/p3_tb/p3_top/qsys_module/qsys[6]/or_req_8_1/i_c}
add wave -noupdate -group or_concat6 {/p3_tb/p3_top/qsys_module/qsys[6]/or_req_8_1/i_d}
add wave -noupdate -group or_concat6 {/p3_tb/p3_top/qsys_module/qsys[6]/or_req_8_1/i_e}
add wave -noupdate -group or_concat6 {/p3_tb/p3_top/qsys_module/qsys[6]/or_req_8_1/i_f}
add wave -noupdate -group or_concat6 {/p3_tb/p3_top/qsys_module/qsys[6]/or_req_8_1/i_g}
add wave -noupdate -group or_concat6 {/p3_tb/p3_top/qsys_module/qsys[6]/or_req_8_1/i_h}
add wave -noupdate -group or_concat6 {/p3_tb/p3_top/qsys_module/qsys[6]/or_req_8_1/o_sltd}
add wave -noupdate -group or_concat7 {/p3_tb/p3_top/qsys_module/qsys[7]/or_req_8_1/i_a}
add wave -noupdate -group or_concat7 {/p3_tb/p3_top/qsys_module/qsys[7]/or_req_8_1/i_b}
add wave -noupdate -group or_concat7 {/p3_tb/p3_top/qsys_module/qsys[7]/or_req_8_1/i_c}
add wave -noupdate -group or_concat7 {/p3_tb/p3_top/qsys_module/qsys[7]/or_req_8_1/i_d}
add wave -noupdate -group or_concat7 {/p3_tb/p3_top/qsys_module/qsys[7]/or_req_8_1/i_e}
add wave -noupdate -group or_concat7 {/p3_tb/p3_top/qsys_module/qsys[7]/or_req_8_1/i_f}
add wave -noupdate -group or_concat7 {/p3_tb/p3_top/qsys_module/qsys[7]/or_req_8_1/i_g}
add wave -noupdate -group or_concat7 {/p3_tb/p3_top/qsys_module/qsys[7]/or_req_8_1/i_h}
add wave -noupdate -group or_concat7 {/p3_tb/p3_top/qsys_module/qsys[7]/or_req_8_1/o_sltd}
add wave -noupdate -divider WCRR_EX_REQ
add wave -noupdate -group wcrr_ex_req0 {/p3_tb/p3_top/qsys_module/qsys[0]/arb_ack_mux/i_a}
add wave -noupdate -group wcrr_ex_req0 {/p3_tb/p3_top/qsys_module/qsys[0]/arb_ack_mux/i_b}
add wave -noupdate -group wcrr_ex_req0 {/p3_tb/p3_top/qsys_module/qsys[0]/arb_ack_mux/i_c}
add wave -noupdate -group wcrr_ex_req0 {/p3_tb/p3_top/qsys_module/qsys[0]/arb_ack_mux/i_d}
add wave -noupdate -group wcrr_ex_req0 {/p3_tb/p3_top/qsys_module/qsys[0]/arb_ack_mux/i_e}
add wave -noupdate -group wcrr_ex_req0 {/p3_tb/p3_top/qsys_module/qsys[0]/arb_ack_mux/i_f}
add wave -noupdate -group wcrr_ex_req0 {/p3_tb/p3_top/qsys_module/qsys[0]/arb_ack_mux/i_g}
add wave -noupdate -group wcrr_ex_req0 {/p3_tb/p3_top/qsys_module/qsys[0]/arb_ack_mux/i_h}
add wave -noupdate -group wcrr_ex_req0 {/p3_tb/p3_top/qsys_module/qsys[0]/arb_ack_mux/i_sel}
add wave -noupdate -group wcrr_ex_req0 {/p3_tb/p3_top/qsys_module/qsys[0]/arb_ack_mux/o_sltd}
add wave -noupdate -group wcrr_ex_req1 {/p3_tb/p3_top/qsys_module/qsys[1]/arb_ack_mux/i_a}
add wave -noupdate -group wcrr_ex_req1 {/p3_tb/p3_top/qsys_module/qsys[1]/arb_ack_mux/i_b}
add wave -noupdate -group wcrr_ex_req1 {/p3_tb/p3_top/qsys_module/qsys[1]/arb_ack_mux/i_c}
add wave -noupdate -group wcrr_ex_req1 {/p3_tb/p3_top/qsys_module/qsys[1]/arb_ack_mux/i_d}
add wave -noupdate -group wcrr_ex_req1 {/p3_tb/p3_top/qsys_module/qsys[1]/arb_ack_mux/i_e}
add wave -noupdate -group wcrr_ex_req1 {/p3_tb/p3_top/qsys_module/qsys[1]/arb_ack_mux/i_f}
add wave -noupdate -group wcrr_ex_req1 {/p3_tb/p3_top/qsys_module/qsys[1]/arb_ack_mux/i_g}
add wave -noupdate -group wcrr_ex_req1 {/p3_tb/p3_top/qsys_module/qsys[1]/arb_ack_mux/i_h}
add wave -noupdate -group wcrr_ex_req1 {/p3_tb/p3_top/qsys_module/qsys[1]/arb_ack_mux/i_sel}
add wave -noupdate -group wcrr_ex_req1 {/p3_tb/p3_top/qsys_module/qsys[1]/arb_ack_mux/o_sltd}
add wave -noupdate -group wcrr_ex_req2 {/p3_tb/p3_top/qsys_module/qsys[2]/arb_ack_mux/i_a}
add wave -noupdate -group wcrr_ex_req2 {/p3_tb/p3_top/qsys_module/qsys[2]/arb_ack_mux/i_b}
add wave -noupdate -group wcrr_ex_req2 {/p3_tb/p3_top/qsys_module/qsys[2]/arb_ack_mux/i_c}
add wave -noupdate -group wcrr_ex_req2 {/p3_tb/p3_top/qsys_module/qsys[2]/arb_ack_mux/i_d}
add wave -noupdate -group wcrr_ex_req2 {/p3_tb/p3_top/qsys_module/qsys[2]/arb_ack_mux/i_e}
add wave -noupdate -group wcrr_ex_req2 {/p3_tb/p3_top/qsys_module/qsys[2]/arb_ack_mux/i_f}
add wave -noupdate -group wcrr_ex_req2 {/p3_tb/p3_top/qsys_module/qsys[2]/arb_ack_mux/i_g}
add wave -noupdate -group wcrr_ex_req2 {/p3_tb/p3_top/qsys_module/qsys[2]/arb_ack_mux/i_h}
add wave -noupdate -group wcrr_ex_req2 {/p3_tb/p3_top/qsys_module/qsys[2]/arb_ack_mux/i_sel}
add wave -noupdate -group wcrr_ex_req2 {/p3_tb/p3_top/qsys_module/qsys[2]/arb_ack_mux/o_sltd}
add wave -noupdate -group wcrr_ex_req3 {/p3_tb/p3_top/qsys_module/qsys[3]/arb_ack_mux/i_a}
add wave -noupdate -group wcrr_ex_req3 {/p3_tb/p3_top/qsys_module/qsys[3]/arb_ack_mux/i_b}
add wave -noupdate -group wcrr_ex_req3 {/p3_tb/p3_top/qsys_module/qsys[3]/arb_ack_mux/i_c}
add wave -noupdate -group wcrr_ex_req3 {/p3_tb/p3_top/qsys_module/qsys[3]/arb_ack_mux/i_d}
add wave -noupdate -group wcrr_ex_req3 {/p3_tb/p3_top/qsys_module/qsys[3]/arb_ack_mux/i_e}
add wave -noupdate -group wcrr_ex_req3 {/p3_tb/p3_top/qsys_module/qsys[3]/arb_ack_mux/i_f}
add wave -noupdate -group wcrr_ex_req3 {/p3_tb/p3_top/qsys_module/qsys[3]/arb_ack_mux/i_g}
add wave -noupdate -group wcrr_ex_req3 {/p3_tb/p3_top/qsys_module/qsys[3]/arb_ack_mux/i_h}
add wave -noupdate -group wcrr_ex_req3 {/p3_tb/p3_top/qsys_module/qsys[3]/arb_ack_mux/i_sel}
add wave -noupdate -group wcrr_ex_req3 {/p3_tb/p3_top/qsys_module/qsys[3]/arb_ack_mux/o_sltd}
add wave -noupdate -group wcrr_ex_req4 {/p3_tb/p3_top/qsys_module/qsys[4]/arb_ack_mux/i_a}
add wave -noupdate -group wcrr_ex_req4 {/p3_tb/p3_top/qsys_module/qsys[4]/arb_ack_mux/i_b}
add wave -noupdate -group wcrr_ex_req4 {/p3_tb/p3_top/qsys_module/qsys[4]/arb_ack_mux/i_c}
add wave -noupdate -group wcrr_ex_req4 {/p3_tb/p3_top/qsys_module/qsys[4]/arb_ack_mux/i_d}
add wave -noupdate -group wcrr_ex_req4 {/p3_tb/p3_top/qsys_module/qsys[4]/arb_ack_mux/i_e}
add wave -noupdate -group wcrr_ex_req4 {/p3_tb/p3_top/qsys_module/qsys[4]/arb_ack_mux/i_f}
add wave -noupdate -group wcrr_ex_req4 {/p3_tb/p3_top/qsys_module/qsys[4]/arb_ack_mux/i_g}
add wave -noupdate -group wcrr_ex_req4 {/p3_tb/p3_top/qsys_module/qsys[4]/arb_ack_mux/i_h}
add wave -noupdate -group wcrr_ex_req4 {/p3_tb/p3_top/qsys_module/qsys[4]/arb_ack_mux/i_sel}
add wave -noupdate -group wcrr_ex_req4 {/p3_tb/p3_top/qsys_module/qsys[4]/arb_ack_mux/o_sltd}
add wave -noupdate -group wcrr_ex_req5 {/p3_tb/p3_top/qsys_module/qsys[5]/arb_ack_mux/i_a}
add wave -noupdate -group wcrr_ex_req5 {/p3_tb/p3_top/qsys_module/qsys[5]/arb_ack_mux/i_b}
add wave -noupdate -group wcrr_ex_req5 {/p3_tb/p3_top/qsys_module/qsys[5]/arb_ack_mux/i_c}
add wave -noupdate -group wcrr_ex_req5 {/p3_tb/p3_top/qsys_module/qsys[5]/arb_ack_mux/i_d}
add wave -noupdate -group wcrr_ex_req5 {/p3_tb/p3_top/qsys_module/qsys[5]/arb_ack_mux/i_e}
add wave -noupdate -group wcrr_ex_req5 {/p3_tb/p3_top/qsys_module/qsys[5]/arb_ack_mux/i_f}
add wave -noupdate -group wcrr_ex_req5 {/p3_tb/p3_top/qsys_module/qsys[5]/arb_ack_mux/i_g}
add wave -noupdate -group wcrr_ex_req5 {/p3_tb/p3_top/qsys_module/qsys[5]/arb_ack_mux/i_h}
add wave -noupdate -group wcrr_ex_req5 {/p3_tb/p3_top/qsys_module/qsys[5]/arb_ack_mux/i_sel}
add wave -noupdate -group wcrr_ex_req5 {/p3_tb/p3_top/qsys_module/qsys[5]/arb_ack_mux/o_sltd}
add wave -noupdate -group wcrr_ex_req6 {/p3_tb/p3_top/qsys_module/qsys[6]/arb_ack_mux/i_a}
add wave -noupdate -group wcrr_ex_req6 {/p3_tb/p3_top/qsys_module/qsys[6]/arb_ack_mux/i_b}
add wave -noupdate -group wcrr_ex_req6 {/p3_tb/p3_top/qsys_module/qsys[6]/arb_ack_mux/i_c}
add wave -noupdate -group wcrr_ex_req6 {/p3_tb/p3_top/qsys_module/qsys[6]/arb_ack_mux/i_d}
add wave -noupdate -group wcrr_ex_req6 {/p3_tb/p3_top/qsys_module/qsys[6]/arb_ack_mux/i_e}
add wave -noupdate -group wcrr_ex_req6 {/p3_tb/p3_top/qsys_module/qsys[6]/arb_ack_mux/i_f}
add wave -noupdate -group wcrr_ex_req6 {/p3_tb/p3_top/qsys_module/qsys[6]/arb_ack_mux/i_g}
add wave -noupdate -group wcrr_ex_req6 {/p3_tb/p3_top/qsys_module/qsys[6]/arb_ack_mux/i_h}
add wave -noupdate -group wcrr_ex_req6 {/p3_tb/p3_top/qsys_module/qsys[6]/arb_ack_mux/i_sel}
add wave -noupdate -group wcrr_ex_req6 {/p3_tb/p3_top/qsys_module/qsys[6]/arb_ack_mux/o_sltd}
add wave -noupdate -group wcrr_ex_req7 {/p3_tb/p3_top/qsys_module/qsys[7]/arb_ack_mux/i_a}
add wave -noupdate -group wcrr_ex_req7 {/p3_tb/p3_top/qsys_module/qsys[7]/arb_ack_mux/i_b}
add wave -noupdate -group wcrr_ex_req7 {/p3_tb/p3_top/qsys_module/qsys[7]/arb_ack_mux/i_c}
add wave -noupdate -group wcrr_ex_req7 {/p3_tb/p3_top/qsys_module/qsys[7]/arb_ack_mux/i_d}
add wave -noupdate -group wcrr_ex_req7 {/p3_tb/p3_top/qsys_module/qsys[7]/arb_ack_mux/i_e}
add wave -noupdate -group wcrr_ex_req7 {/p3_tb/p3_top/qsys_module/qsys[7]/arb_ack_mux/i_f}
add wave -noupdate -group wcrr_ex_req7 {/p3_tb/p3_top/qsys_module/qsys[7]/arb_ack_mux/i_g}
add wave -noupdate -group wcrr_ex_req7 {/p3_tb/p3_top/qsys_module/qsys[7]/arb_ack_mux/i_h}
add wave -noupdate -group wcrr_ex_req7 {/p3_tb/p3_top/qsys_module/qsys[7]/arb_ack_mux/i_sel}
add wave -noupdate -group wcrr_ex_req7 {/p3_tb/p3_top/qsys_module/qsys[7]/arb_ack_mux/o_sltd}
add wave -noupdate -divider {MUX WCRR}
add wave -noupdate -group mux_wcrr0 {/p3_tb/p3_top/qsys_module/qsys[0]/mux_wcrr_8_1/i_a}
add wave -noupdate -group mux_wcrr0 {/p3_tb/p3_top/qsys_module/qsys[0]/mux_wcrr_8_1/i_b}
add wave -noupdate -group mux_wcrr0 {/p3_tb/p3_top/qsys_module/qsys[0]/mux_wcrr_8_1/i_c}
add wave -noupdate -group mux_wcrr0 {/p3_tb/p3_top/qsys_module/qsys[0]/mux_wcrr_8_1/i_d}
add wave -noupdate -group mux_wcrr0 {/p3_tb/p3_top/qsys_module/qsys[0]/mux_wcrr_8_1/i_e}
add wave -noupdate -group mux_wcrr0 {/p3_tb/p3_top/qsys_module/qsys[0]/mux_wcrr_8_1/i_f}
add wave -noupdate -group mux_wcrr0 {/p3_tb/p3_top/qsys_module/qsys[0]/mux_wcrr_8_1/i_g}
add wave -noupdate -group mux_wcrr0 {/p3_tb/p3_top/qsys_module/qsys[0]/mux_wcrr_8_1/i_h}
add wave -noupdate -group mux_wcrr0 {/p3_tb/p3_top/qsys_module/qsys[0]/mux_wcrr_8_1/i_sel}
add wave -noupdate -group mux_wcrr0 {/p3_tb/p3_top/qsys_module/qsys[0]/mux_wcrr_8_1/o_sltd}
add wave -noupdate -group mux_wcrr1 {/p3_tb/p3_top/qsys_module/qsys[1]/mux_wcrr_8_1/i_a}
add wave -noupdate -group mux_wcrr1 {/p3_tb/p3_top/qsys_module/qsys[1]/mux_wcrr_8_1/i_b}
add wave -noupdate -group mux_wcrr1 {/p3_tb/p3_top/qsys_module/qsys[1]/mux_wcrr_8_1/i_c}
add wave -noupdate -group mux_wcrr1 {/p3_tb/p3_top/qsys_module/qsys[1]/mux_wcrr_8_1/i_d}
add wave -noupdate -group mux_wcrr1 {/p3_tb/p3_top/qsys_module/qsys[1]/mux_wcrr_8_1/i_e}
add wave -noupdate -group mux_wcrr1 {/p3_tb/p3_top/qsys_module/qsys[1]/mux_wcrr_8_1/i_f}
add wave -noupdate -group mux_wcrr1 {/p3_tb/p3_top/qsys_module/qsys[1]/mux_wcrr_8_1/i_g}
add wave -noupdate -group mux_wcrr1 {/p3_tb/p3_top/qsys_module/qsys[1]/mux_wcrr_8_1/i_h}
add wave -noupdate -group mux_wcrr1 {/p3_tb/p3_top/qsys_module/qsys[1]/mux_wcrr_8_1/i_sel}
add wave -noupdate -group mux_wcrr1 {/p3_tb/p3_top/qsys_module/qsys[1]/mux_wcrr_8_1/o_sltd}
add wave -noupdate -group mux_wcrr2 {/p3_tb/p3_top/qsys_module/qsys[2]/mux_wcrr_8_1/i_a}
add wave -noupdate -group mux_wcrr2 {/p3_tb/p3_top/qsys_module/qsys[2]/mux_wcrr_8_1/i_b}
add wave -noupdate -group mux_wcrr2 {/p3_tb/p3_top/qsys_module/qsys[2]/mux_wcrr_8_1/i_c}
add wave -noupdate -group mux_wcrr2 {/p3_tb/p3_top/qsys_module/qsys[2]/mux_wcrr_8_1/i_d}
add wave -noupdate -group mux_wcrr2 {/p3_tb/p3_top/qsys_module/qsys[2]/mux_wcrr_8_1/i_e}
add wave -noupdate -group mux_wcrr2 {/p3_tb/p3_top/qsys_module/qsys[2]/mux_wcrr_8_1/i_f}
add wave -noupdate -group mux_wcrr2 {/p3_tb/p3_top/qsys_module/qsys[2]/mux_wcrr_8_1/i_g}
add wave -noupdate -group mux_wcrr2 {/p3_tb/p3_top/qsys_module/qsys[2]/mux_wcrr_8_1/i_h}
add wave -noupdate -group mux_wcrr2 {/p3_tb/p3_top/qsys_module/qsys[2]/mux_wcrr_8_1/i_sel}
add wave -noupdate -group mux_wcrr2 {/p3_tb/p3_top/qsys_module/qsys[2]/mux_wcrr_8_1/o_sltd}
add wave -noupdate -group mux_wcrr3 {/p3_tb/p3_top/qsys_module/qsys[3]/mux_wcrr_8_1/i_a}
add wave -noupdate -group mux_wcrr3 {/p3_tb/p3_top/qsys_module/qsys[3]/mux_wcrr_8_1/i_b}
add wave -noupdate -group mux_wcrr3 {/p3_tb/p3_top/qsys_module/qsys[3]/mux_wcrr_8_1/i_c}
add wave -noupdate -group mux_wcrr3 {/p3_tb/p3_top/qsys_module/qsys[3]/mux_wcrr_8_1/i_d}
add wave -noupdate -group mux_wcrr3 {/p3_tb/p3_top/qsys_module/qsys[3]/mux_wcrr_8_1/i_e}
add wave -noupdate -group mux_wcrr3 {/p3_tb/p3_top/qsys_module/qsys[3]/mux_wcrr_8_1/i_f}
add wave -noupdate -group mux_wcrr3 {/p3_tb/p3_top/qsys_module/qsys[3]/mux_wcrr_8_1/i_g}
add wave -noupdate -group mux_wcrr3 {/p3_tb/p3_top/qsys_module/qsys[3]/mux_wcrr_8_1/i_h}
add wave -noupdate -group mux_wcrr3 {/p3_tb/p3_top/qsys_module/qsys[3]/mux_wcrr_8_1/i_sel}
add wave -noupdate -group mux_wcrr3 {/p3_tb/p3_top/qsys_module/qsys[3]/mux_wcrr_8_1/o_sltd}
add wave -noupdate -group mux_wcrr4 {/p3_tb/p3_top/qsys_module/qsys[4]/mux_wcrr_8_1/i_a}
add wave -noupdate -group mux_wcrr4 {/p3_tb/p3_top/qsys_module/qsys[4]/mux_wcrr_8_1/i_b}
add wave -noupdate -group mux_wcrr4 {/p3_tb/p3_top/qsys_module/qsys[4]/mux_wcrr_8_1/i_c}
add wave -noupdate -group mux_wcrr4 {/p3_tb/p3_top/qsys_module/qsys[4]/mux_wcrr_8_1/i_d}
add wave -noupdate -group mux_wcrr4 {/p3_tb/p3_top/qsys_module/qsys[4]/mux_wcrr_8_1/i_e}
add wave -noupdate -group mux_wcrr4 {/p3_tb/p3_top/qsys_module/qsys[4]/mux_wcrr_8_1/i_f}
add wave -noupdate -group mux_wcrr4 {/p3_tb/p3_top/qsys_module/qsys[4]/mux_wcrr_8_1/i_g}
add wave -noupdate -group mux_wcrr4 {/p3_tb/p3_top/qsys_module/qsys[4]/mux_wcrr_8_1/i_h}
add wave -noupdate -group mux_wcrr4 {/p3_tb/p3_top/qsys_module/qsys[4]/mux_wcrr_8_1/i_sel}
add wave -noupdate -group mux_wcrr4 {/p3_tb/p3_top/qsys_module/qsys[4]/mux_wcrr_8_1/o_sltd}
add wave -noupdate -group mux_wcrr5 {/p3_tb/p3_top/qsys_module/qsys[5]/mux_wcrr_8_1/i_a}
add wave -noupdate -group mux_wcrr5 {/p3_tb/p3_top/qsys_module/qsys[5]/mux_wcrr_8_1/i_b}
add wave -noupdate -group mux_wcrr5 {/p3_tb/p3_top/qsys_module/qsys[5]/mux_wcrr_8_1/i_c}
add wave -noupdate -group mux_wcrr5 {/p3_tb/p3_top/qsys_module/qsys[5]/mux_wcrr_8_1/i_d}
add wave -noupdate -group mux_wcrr5 {/p3_tb/p3_top/qsys_module/qsys[5]/mux_wcrr_8_1/i_e}
add wave -noupdate -group mux_wcrr5 {/p3_tb/p3_top/qsys_module/qsys[5]/mux_wcrr_8_1/i_f}
add wave -noupdate -group mux_wcrr5 {/p3_tb/p3_top/qsys_module/qsys[5]/mux_wcrr_8_1/i_g}
add wave -noupdate -group mux_wcrr5 {/p3_tb/p3_top/qsys_module/qsys[5]/mux_wcrr_8_1/i_h}
add wave -noupdate -group mux_wcrr5 {/p3_tb/p3_top/qsys_module/qsys[5]/mux_wcrr_8_1/i_sel}
add wave -noupdate -group mux_wcrr5 {/p3_tb/p3_top/qsys_module/qsys[5]/mux_wcrr_8_1/o_sltd}
add wave -noupdate -group mux_wcrr6 {/p3_tb/p3_top/qsys_module/qsys[6]/mux_wcrr_8_1/i_a}
add wave -noupdate -group mux_wcrr6 {/p3_tb/p3_top/qsys_module/qsys[6]/mux_wcrr_8_1/i_b}
add wave -noupdate -group mux_wcrr6 {/p3_tb/p3_top/qsys_module/qsys[6]/mux_wcrr_8_1/i_c}
add wave -noupdate -group mux_wcrr6 {/p3_tb/p3_top/qsys_module/qsys[6]/mux_wcrr_8_1/i_d}
add wave -noupdate -group mux_wcrr6 {/p3_tb/p3_top/qsys_module/qsys[6]/mux_wcrr_8_1/i_e}
add wave -noupdate -group mux_wcrr6 {/p3_tb/p3_top/qsys_module/qsys[6]/mux_wcrr_8_1/i_f}
add wave -noupdate -group mux_wcrr6 {/p3_tb/p3_top/qsys_module/qsys[6]/mux_wcrr_8_1/i_g}
add wave -noupdate -group mux_wcrr6 {/p3_tb/p3_top/qsys_module/qsys[6]/mux_wcrr_8_1/i_h}
add wave -noupdate -group mux_wcrr6 {/p3_tb/p3_top/qsys_module/qsys[6]/mux_wcrr_8_1/i_sel}
add wave -noupdate -group mux_wcrr6 {/p3_tb/p3_top/qsys_module/qsys[6]/mux_wcrr_8_1/o_sltd}
add wave -noupdate -group mux_wcrr7 {/p3_tb/p3_top/qsys_module/qsys[7]/mux_wcrr_8_1/i_a}
add wave -noupdate -group mux_wcrr7 {/p3_tb/p3_top/qsys_module/qsys[7]/mux_wcrr_8_1/i_b}
add wave -noupdate -group mux_wcrr7 {/p3_tb/p3_top/qsys_module/qsys[7]/mux_wcrr_8_1/i_c}
add wave -noupdate -group mux_wcrr7 {/p3_tb/p3_top/qsys_module/qsys[7]/mux_wcrr_8_1/i_d}
add wave -noupdate -group mux_wcrr7 {/p3_tb/p3_top/qsys_module/qsys[7]/mux_wcrr_8_1/i_e}
add wave -noupdate -group mux_wcrr7 {/p3_tb/p3_top/qsys_module/qsys[7]/mux_wcrr_8_1/i_f}
add wave -noupdate -group mux_wcrr7 {/p3_tb/p3_top/qsys_module/qsys[7]/mux_wcrr_8_1/i_g}
add wave -noupdate -group mux_wcrr7 {/p3_tb/p3_top/qsys_module/qsys[7]/mux_wcrr_8_1/i_h}
add wave -noupdate -group mux_wcrr7 {/p3_tb/p3_top/qsys_module/qsys[7]/mux_wcrr_8_1/i_sel}
add wave -noupdate -group mux_wcrr7 {/p3_tb/p3_top/qsys_module/qsys[7]/mux_wcrr_8_1/o_sltd}
add wave -noupdate -divider OFIFO
add wave -noupdate -radix decimal /p3_tb/p3_top/uart_module/seq_decode/cmd_detector/o_data
add wave -noupdate -expand -group oFIFO0 {/p3_tb/p3_top/qsys_module/qsys[0]/oFIFO/push}
add wave -noupdate -expand -group oFIFO0 {/p3_tb/p3_top/qsys_module/qsys[0]/oFIFO/pop}
add wave -noupdate -expand -group oFIFO0 {/p3_tb/p3_top/qsys_module/qsys[0]/oFIFO/restore_pointers}
add wave -noupdate -expand -group oFIFO0 -radix ascii {/p3_tb/p3_top/qsys_module/qsys[0]/oFIFO/DataInput}
add wave -noupdate -expand -group oFIFO0 -radix ascii {/p3_tb/p3_top/qsys_module/qsys[0]/oFIFO/DataOutput}
add wave -noupdate -expand -group oFIFO0 {/p3_tb/p3_top/qsys_module/qsys[0]/oFIFO/full}
add wave -noupdate -expand -group oFIFO0 {/p3_tb/p3_top/qsys_module/qsys[0]/oFIFO/empty}
add wave -noupdate -expand -group oFIFO0 {/p3_tb/p3_top/qsys_module/qsys[0]/oFIFO/ram/ram}
add wave -noupdate -group oFIFO1 {/p3_tb/p3_top/qsys_module/qsys[1]/oFIFO/push}
add wave -noupdate -group oFIFO1 {/p3_tb/p3_top/qsys_module/qsys[1]/oFIFO/pop}
add wave -noupdate -group oFIFO1 {/p3_tb/p3_top/qsys_module/qsys[1]/oFIFO/restore_pointers}
add wave -noupdate -group oFIFO1 -radix ascii {/p3_tb/p3_top/qsys_module/qsys[1]/oFIFO/DataInput}
add wave -noupdate -group oFIFO1 -radix ascii {/p3_tb/p3_top/qsys_module/qsys[1]/oFIFO/DataOutput}
add wave -noupdate -group oFIFO1 {/p3_tb/p3_top/qsys_module/qsys[1]/oFIFO/full}
add wave -noupdate -group oFIFO1 {/p3_tb/p3_top/qsys_module/qsys[1]/oFIFO/empty}
add wave -noupdate -group oFIFO2 {/p3_tb/p3_top/qsys_module/qsys[2]/oFIFO/push}
add wave -noupdate -group oFIFO2 {/p3_tb/p3_top/qsys_module/qsys[2]/oFIFO/pop}
add wave -noupdate -group oFIFO2 {/p3_tb/p3_top/qsys_module/qsys[2]/oFIFO/restore_pointers}
add wave -noupdate -group oFIFO2 -radix ascii {/p3_tb/p3_top/qsys_module/qsys[2]/oFIFO/DataInput}
add wave -noupdate -group oFIFO2 -radix ascii {/p3_tb/p3_top/qsys_module/qsys[2]/oFIFO/DataOutput}
add wave -noupdate -group oFIFO2 {/p3_tb/p3_top/qsys_module/qsys[2]/oFIFO/full}
add wave -noupdate -group oFIFO2 {/p3_tb/p3_top/qsys_module/qsys[2]/oFIFO/empty}
add wave -noupdate -group oFIFO3 {/p3_tb/p3_top/qsys_module/qsys[3]/oFIFO/push}
add wave -noupdate -group oFIFO3 {/p3_tb/p3_top/qsys_module/qsys[3]/oFIFO/pop}
add wave -noupdate -group oFIFO3 {/p3_tb/p3_top/qsys_module/qsys[3]/oFIFO/restore_pointers}
add wave -noupdate -group oFIFO3 -radix ascii {/p3_tb/p3_top/qsys_module/qsys[3]/oFIFO/DataInput}
add wave -noupdate -group oFIFO3 -radix ascii {/p3_tb/p3_top/qsys_module/qsys[3]/oFIFO/DataOutput}
add wave -noupdate -group oFIFO3 {/p3_tb/p3_top/qsys_module/qsys[3]/oFIFO/full}
add wave -noupdate -group oFIFO3 {/p3_tb/p3_top/qsys_module/qsys[3]/oFIFO/empty}
add wave -noupdate -group oFIFO4 {/p3_tb/p3_top/qsys_module/qsys[4]/oFIFO/push}
add wave -noupdate -group oFIFO4 {/p3_tb/p3_top/qsys_module/qsys[4]/oFIFO/pop}
add wave -noupdate -group oFIFO4 {/p3_tb/p3_top/qsys_module/qsys[4]/oFIFO/restore_pointers}
add wave -noupdate -group oFIFO4 {/p3_tb/p3_top/qsys_module/qsys[4]/oFIFO/DataInput}
add wave -noupdate -group oFIFO4 {/p3_tb/p3_top/qsys_module/qsys[4]/oFIFO/DataOutput}
add wave -noupdate -group oFIFO4 {/p3_tb/p3_top/qsys_module/qsys[4]/oFIFO/full}
add wave -noupdate -group oFIFO4 {/p3_tb/p3_top/qsys_module/qsys[4]/oFIFO/empty}
add wave -noupdate -group oFIFO5 {/p3_tb/p3_top/qsys_module/qsys[5]/oFIFO/push}
add wave -noupdate -group oFIFO5 {/p3_tb/p3_top/qsys_module/qsys[5]/oFIFO/pop}
add wave -noupdate -group oFIFO5 {/p3_tb/p3_top/qsys_module/qsys[5]/oFIFO/restore_pointers}
add wave -noupdate -group oFIFO5 {/p3_tb/p3_top/qsys_module/qsys[5]/oFIFO/DataInput}
add wave -noupdate -group oFIFO5 {/p3_tb/p3_top/qsys_module/qsys[5]/oFIFO/DataOutput}
add wave -noupdate -group oFIFO5 {/p3_tb/p3_top/qsys_module/qsys[5]/oFIFO/full}
add wave -noupdate -group oFIFO5 {/p3_tb/p3_top/qsys_module/qsys[5]/oFIFO/empty}
add wave -noupdate -group oFIFO6 {/p3_tb/p3_top/qsys_module/qsys[6]/oFIFO/push}
add wave -noupdate -group oFIFO6 {/p3_tb/p3_top/qsys_module/qsys[6]/oFIFO/pop}
add wave -noupdate -group oFIFO6 {/p3_tb/p3_top/qsys_module/qsys[6]/oFIFO/restore_pointers}
add wave -noupdate -group oFIFO6 -radix ascii {/p3_tb/p3_top/qsys_module/qsys[6]/oFIFO/DataInput}
add wave -noupdate -group oFIFO6 -radix ascii {/p3_tb/p3_top/qsys_module/qsys[6]/oFIFO/DataOutput}
add wave -noupdate -group oFIFO6 {/p3_tb/p3_top/qsys_module/qsys[6]/oFIFO/full}
add wave -noupdate -group oFIFO6 {/p3_tb/p3_top/qsys_module/qsys[6]/oFIFO/empty}
add wave -noupdate -group oFIFO7 {/p3_tb/p3_top/qsys_module/qsys[7]/oFIFO/push}
add wave -noupdate -group oFIFO7 {/p3_tb/p3_top/qsys_module/qsys[7]/oFIFO/pop}
add wave -noupdate -group oFIFO7 {/p3_tb/p3_top/qsys_module/qsys[7]/oFIFO/restore_pointers}
add wave -noupdate -group oFIFO7 {/p3_tb/p3_top/qsys_module/qsys[7]/oFIFO/DataInput}
add wave -noupdate -group oFIFO7 {/p3_tb/p3_top/qsys_module/qsys[7]/oFIFO/DataOutput}
add wave -noupdate -group oFIFO7 {/p3_tb/p3_top/qsys_module/qsys[7]/oFIFO/full}
add wave -noupdate -group oFIFO7 {/p3_tb/p3_top/qsys_module/qsys[7]/oFIFO/empty}
add wave -noupdate -divider BACKUP
add wave -noupdate -radix decimal /p3_tb/p3_top/uart_module/seq_decode/cmd_detector/o_data
add wave -noupdate /p3_tb/p3_top/qsys_module/transmit_processing
add wave -noupdate /p3_tb/p3_top/qsys_module/retransmit_processing
add wave -noupdate -group {pop control} /p3_tb/p3_top/qsys_module/qsys_transmit/transmit_control/enb_transmit
add wave -noupdate -group {pop control} /p3_tb/p3_top/qsys_module/qsys_transmit/transmit_control/empty_fifo0
add wave -noupdate -group {pop control} /p3_tb/p3_top/qsys_module/qsys_transmit/transmit_control/empty_fifo1
add wave -noupdate -group {pop control} /p3_tb/p3_top/qsys_module/qsys_transmit/transmit_control/empty_fifo2
add wave -noupdate -group {pop control} /p3_tb/p3_top/qsys_module/qsys_transmit/transmit_control/empty_fifo3
add wave -noupdate -group {pop control} /p3_tb/p3_top/qsys_module/qsys_transmit/transmit_control/empty_fifo4
add wave -noupdate -group {pop control} /p3_tb/p3_top/qsys_module/qsys_transmit/transmit_control/empty_fifo5
add wave -noupdate -group {pop control} /p3_tb/p3_top/qsys_module/qsys_transmit/transmit_control/empty_fifo6
add wave -noupdate -group {pop control} /p3_tb/p3_top/qsys_module/qsys_transmit/transmit_control/empty_fifo7
add wave -noupdate -group {pop control} /p3_tb/p3_top/qsys_module/qsys_transmit/transmit_control/pop_fifo0
add wave -noupdate -group {pop control} /p3_tb/p3_top/qsys_module/qsys_transmit/transmit_control/pop_fifo1
add wave -noupdate -group {pop control} /p3_tb/p3_top/qsys_module/qsys_transmit/transmit_control/pop_fifo2
add wave -noupdate -group {pop control} /p3_tb/p3_top/qsys_module/qsys_transmit/transmit_control/pop_fifo3
add wave -noupdate -group {pop control} /p3_tb/p3_top/qsys_module/qsys_transmit/transmit_control/pop_fifo4
add wave -noupdate -group {pop control} /p3_tb/p3_top/qsys_module/qsys_transmit/transmit_control/pop_fifo5
add wave -noupdate -group {pop control} /p3_tb/p3_top/qsys_module/qsys_transmit/transmit_control/pop_fifo6
add wave -noupdate -group {pop control} /p3_tb/p3_top/qsys_module/qsys_transmit/transmit_control/pop_fifo7
add wave -noupdate -group {pop control} /p3_tb/p3_top/qsys_module/qsys_transmit/transmit_control/o_mux_selector
add wave -noupdate -group {pop control} /p3_tb/p3_top/qsys_module/qsys_transmit/transmit_control/current_st
add wave -noupdate -group {pop control} /p3_tb/p3_top/qsys_module/qsys_transmit/transmit_control/nxt_state
add wave -noupdate -group backup_fifo0 {/p3_tb/p3_top/qsys_module/qsys[0]/oFIFO_backup/push}
add wave -noupdate -group backup_fifo0 {/p3_tb/p3_top/qsys_module/qsys[0]/oFIFO_backup/pop}
add wave -noupdate -group backup_fifo0 {/p3_tb/p3_top/qsys_module/qsys[0]/oFIFO_backup/restore_pointers}
add wave -noupdate -group backup_fifo0 -radix ascii {/p3_tb/p3_top/qsys_module/qsys[0]/oFIFO_backup/DataInput}
add wave -noupdate -group backup_fifo0 -radix ascii {/p3_tb/p3_top/qsys_module/qsys[0]/oFIFO_backup/DataOutput}
add wave -noupdate -group backup_fifo0 {/p3_tb/p3_top/qsys_module/qsys[0]/oFIFO_backup/full}
add wave -noupdate -group backup_fifo0 {/p3_tb/p3_top/qsys_module/qsys[0]/oFIFO_backup/empty}
add wave -noupdate -group backup_fifo1 {/p3_tb/p3_top/qsys_module/qsys[1]/oFIFO_backup/push}
add wave -noupdate -group backup_fifo1 {/p3_tb/p3_top/qsys_module/qsys[1]/oFIFO_backup/pop}
add wave -noupdate -group backup_fifo1 {/p3_tb/p3_top/qsys_module/qsys[1]/oFIFO_backup/restore_pointers}
add wave -noupdate -group backup_fifo1 -radix ascii {/p3_tb/p3_top/qsys_module/qsys[1]/oFIFO_backup/DataInput}
add wave -noupdate -group backup_fifo1 -radix ascii {/p3_tb/p3_top/qsys_module/qsys[1]/oFIFO_backup/DataOutput}
add wave -noupdate -group backup_fifo1 {/p3_tb/p3_top/qsys_module/qsys[1]/oFIFO_backup/full}
add wave -noupdate -group backup_fifo1 {/p3_tb/p3_top/qsys_module/qsys[1]/oFIFO_backup/empty}
add wave -noupdate -group backup_fifo2 {/p3_tb/p3_top/qsys_module/qsys[2]/oFIFO_backup/push}
add wave -noupdate -group backup_fifo2 {/p3_tb/p3_top/qsys_module/qsys[2]/oFIFO_backup/pop}
add wave -noupdate -group backup_fifo2 {/p3_tb/p3_top/qsys_module/qsys[2]/oFIFO_backup/restore_pointers}
add wave -noupdate -group backup_fifo2 {/p3_tb/p3_top/qsys_module/qsys[2]/oFIFO_backup/DataInput}
add wave -noupdate -group backup_fifo2 {/p3_tb/p3_top/qsys_module/qsys[2]/oFIFO_backup/DataOutput}
add wave -noupdate -group backup_fifo2 {/p3_tb/p3_top/qsys_module/qsys[2]/oFIFO_backup/full}
add wave -noupdate -group backup_fifo2 {/p3_tb/p3_top/qsys_module/qsys[2]/oFIFO_backup/empty}
add wave -noupdate -group backup_fifo3 {/p3_tb/p3_top/qsys_module/qsys[3]/oFIFO_backup/push}
add wave -noupdate -group backup_fifo3 {/p3_tb/p3_top/qsys_module/qsys[3]/oFIFO_backup/pop}
add wave -noupdate -group backup_fifo3 {/p3_tb/p3_top/qsys_module/qsys[3]/oFIFO_backup/restore_pointers}
add wave -noupdate -group backup_fifo3 {/p3_tb/p3_top/qsys_module/qsys[3]/oFIFO_backup/DataInput}
add wave -noupdate -group backup_fifo3 {/p3_tb/p3_top/qsys_module/qsys[3]/oFIFO_backup/DataOutput}
add wave -noupdate -group backup_fifo3 {/p3_tb/p3_top/qsys_module/qsys[3]/oFIFO_backup/full}
add wave -noupdate -group backup_fifo3 {/p3_tb/p3_top/qsys_module/qsys[3]/oFIFO_backup/empty}
add wave -noupdate -group backup_fifo4 {/p3_tb/p3_top/qsys_module/qsys[4]/oFIFO_backup/push}
add wave -noupdate -group backup_fifo4 {/p3_tb/p3_top/qsys_module/qsys[4]/oFIFO_backup/pop}
add wave -noupdate -group backup_fifo4 {/p3_tb/p3_top/qsys_module/qsys[4]/oFIFO_backup/restore_pointers}
add wave -noupdate -group backup_fifo4 {/p3_tb/p3_top/qsys_module/qsys[4]/oFIFO_backup/DataInput}
add wave -noupdate -group backup_fifo4 {/p3_tb/p3_top/qsys_module/qsys[4]/oFIFO_backup/DataOutput}
add wave -noupdate -group backup_fifo4 {/p3_tb/p3_top/qsys_module/qsys[4]/oFIFO_backup/full}
add wave -noupdate -group backup_fifo4 {/p3_tb/p3_top/qsys_module/qsys[4]/oFIFO_backup/empty}
add wave -noupdate -group backup_fifo5 {/p3_tb/p3_top/qsys_module/qsys[5]/oFIFO_backup/push}
add wave -noupdate -group backup_fifo5 {/p3_tb/p3_top/qsys_module/qsys[5]/oFIFO_backup/pop}
add wave -noupdate -group backup_fifo5 {/p3_tb/p3_top/qsys_module/qsys[5]/oFIFO_backup/restore_pointers}
add wave -noupdate -group backup_fifo5 {/p3_tb/p3_top/qsys_module/qsys[5]/oFIFO_backup/DataInput}
add wave -noupdate -group backup_fifo5 {/p3_tb/p3_top/qsys_module/qsys[5]/oFIFO_backup/DataOutput}
add wave -noupdate -group backup_fifo5 {/p3_tb/p3_top/qsys_module/qsys[5]/oFIFO_backup/full}
add wave -noupdate -group backup_fifo5 {/p3_tb/p3_top/qsys_module/qsys[5]/oFIFO_backup/empty}
add wave -noupdate -group backup_fifo6 {/p3_tb/p3_top/qsys_module/qsys[6]/oFIFO_backup/push}
add wave -noupdate -group backup_fifo6 {/p3_tb/p3_top/qsys_module/qsys[6]/oFIFO_backup/pop}
add wave -noupdate -group backup_fifo6 {/p3_tb/p3_top/qsys_module/qsys[6]/oFIFO_backup/restore_pointers}
add wave -noupdate -group backup_fifo6 {/p3_tb/p3_top/qsys_module/qsys[6]/oFIFO_backup/DataInput}
add wave -noupdate -group backup_fifo6 {/p3_tb/p3_top/qsys_module/qsys[6]/oFIFO_backup/DataOutput}
add wave -noupdate -group backup_fifo6 {/p3_tb/p3_top/qsys_module/qsys[6]/oFIFO_backup/full}
add wave -noupdate -group backup_fifo6 {/p3_tb/p3_top/qsys_module/qsys[6]/oFIFO_backup/empty}
add wave -noupdate -group backup_fifo7 {/p3_tb/p3_top/qsys_module/qsys[7]/oFIFO_backup/push}
add wave -noupdate -group backup_fifo7 {/p3_tb/p3_top/qsys_module/qsys[7]/oFIFO_backup/pop}
add wave -noupdate -group backup_fifo7 {/p3_tb/p3_top/qsys_module/qsys[7]/oFIFO_backup/restore_pointers}
add wave -noupdate -group backup_fifo7 {/p3_tb/p3_top/qsys_module/qsys[7]/oFIFO_backup/DataInput}
add wave -noupdate -group backup_fifo7 {/p3_tb/p3_top/qsys_module/qsys[7]/oFIFO_backup/DataOutput}
add wave -noupdate -group backup_fifo7 {/p3_tb/p3_top/qsys_module/qsys[7]/oFIFO_backup/full}
add wave -noupdate -group backup_fifo7 {/p3_tb/p3_top/qsys_module/qsys[7]/oFIFO_backup/empty}
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {46411000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 471
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {46320959 ps} {46501041 ps}
