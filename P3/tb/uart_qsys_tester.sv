`ifndef SDP_DC_RAM__PKG_SV
    `define SDP_DC_RAM_PKG_SV

import port_pkg::*;
import fifo_pkg::*;
import data_pkg::*;

package uart_qsys_tester;
    class uart_qsys_tester;
        localparam array_size = 16;
        localparam DW_G= 8;
        localparam PERIOD = 2;
        logic [3:0] src;
        logic [3:0] dest;
        logic [3:0] length;
        logic [array_size-1:0][DW_G-1:0] data[2**8:0];
        logic [8:0] data_counter;
	    
	    logic [4:0] g_ports;

        virtual uart_qsys_if qsysif;

        function new(virtual uart_qsys_if vif);
            this.qsysif = vif;
            data_counter = 'd0;
        endfunction

        task init_signals();
            $display("tester signals initialization");
            qsysif.data_tx = 8'd0;
	        qsysif.transmit = 1'b0;
        endtask

        function void set_data_2_send(input logic [3:0]  data_index, input logic [array_size-1:0][DW_G-1:0] i_data);
            if(data_counter < 4'd15) begin
                $display("RAM[%d ] = %s", data_counter, i_data);
                data[data_index] = i_data;
                data_counter +=1;
            end
            else begin
                $error("DATA MEMORY IS FULL");
            end
        endfunction

        function [array_size-1:0][DW_G-1:0] get_data_2_send(input logic [3:0] index);
            if(index < 4'd15) 
                return data[index];
            else
                $fatal("INVALID INDEX");
        endfunction

        task display_stored_data();
            int i;
            for(i=0; i < data_counter; i++) begin
                $display("%s\n\r", data[data_counter]);
            end
        endtask

        task command_0x01(input logic [4:0] n_ports);
            if((n_ports <= 8) && (n_ports >= 0)) begin
                $display("CMD 0x01: %.2d input ports set", n_ports);
	            
	            g_ports = n_ports;
	            
                #PERIOD qsysif.data_tx = 8'hFE;
	            transmit();
	            #PERIOD qsysif.data_tx = 8'h03;
	            transmit();
	            #PERIOD qsysif.data_tx = 8'h01;
	            transmit();
	            #PERIOD qsysif.data_tx = n_ports;
	            transmit();
	            #PERIOD qsysif.data_tx = 8'hEF;
	            transmit();
            end
            else begin
                $fatal("command_0x01: invalid number of input ports");
            end
        endtask

        task command_0x02();
	            
            #PERIOD qsysif.data_tx = 8'hFE;
            transmit();
            #PERIOD qsysif.data_tx = 8'h02;
            transmit();
            #PERIOD qsysif.data_tx = 8'h02;
            transmit();
            #PERIOD qsysif.data_tx = 8'hEF;
            transmit();
        endtask


        //brief: load data to a dest port
        task command_0x04(input logic [3:0] src, input logic [3:0] dest, input logic [array_size-1:0][DW_G-1:0] data);
            int i;
            logic [3:0] length;

            length = getDataArrayLength(data);

            $display("%d",g_ports);
            if((src > g_ports) || (dest > g_ports) || (length <= 0)) begin
                $fatal("command_0x04: number of input ports is not enough to complete this command");
            end
            /* set_dest_and_length */
            @(posedge qsysif.clk)
            #PERIOD qsysif.data_tx = 8'hFE;
            transmit();
            #PERIOD qsysif.data_tx = length;
            transmit();
            #PERIOD qsysif.data_tx = 8'h04;
            transmit();
            #PERIOD qsysif.data_tx = src;
            transmit();
            #PERIOD qsysif.data_tx = dest;
            transmit();
            #PERIOD qsysif.data_tx = src;
            transmit();
            
            for(i=length-1; i >= 0; i--) begin
                @(posedge qsysif.clk)
                #PERIOD qsysif.data_tx = data[i];
                transmit();
            end
            
            #PERIOD qsysif.data_tx = 8'hEF;
            transmit();
        endtask

        //brief: enable data proccesing
        task command_0x05();
	        
            #PERIOD qsysif.data_tx = 8'hFE;
            transmit();
            #PERIOD qsysif.data_tx = 8'h02;
            transmit();
            #PERIOD qsysif.data_tx = 8'h05;
            transmit();
            #PERIOD qsysif.data_tx = 8'hEF;
            transmit();
        endtask

        task command_0x06();
	        
            #PERIOD qsysif.data_tx = 8'hFE;
            transmit();
            #PERIOD qsysif.data_tx = 8'h02;
            transmit();
            #PERIOD qsysif.data_tx = 8'h06;
            transmit();
            #PERIOD qsysif.data_tx = 8'hEF;
            transmit();
        endtask

        function [7:0] getDataArrayLength(logic [array_size-1:0][DW_G-1:0] data);
            logic [7:0] count;
            int k;
            count = 8'd0;
            for(k=0; k < array_size; k++)
            begin
                if(data[k] != 8'd0) begin
                    count = count + 8'd1;
                end
            end
            return count;
        endfunction

        task delay(input logic [7:0] ncycles);
            int i;
            for(i=0; i < ncycles; i++) begin
                #PERIOD;
            end
        endtask
        
        task transmit();
			#PERIOD qsysif.transmit = 1'b0;
			#PERIOD qsysif.transmit = 1'b1; 
			@(posedge qsysif.rts)
			#PERIOD qsysif.transmit = 1'b0;
		endtask

        /* TESTERS */
        task send_data_to_all_ports();
            logic [array_size-1:0][DW_G-1:0] data;
            set_data_2_send(4'd0, "hello");
            set_data_2_send(4'd1, "world");
            set_data_2_send(4'd2,"guys");
            set_data_2_send(4'd3,"this");
            set_data_2_send(4'd4,"is");
            set_data_2_send(4'd5,"a");
            set_data_2_send(4'd6,"test");
            set_data_2_send(4'd7,"!!!!!!");

            /* send data to src port */
            data = get_data_2_send(4'd0);
            command_0x04(4'd0, 4'd0, data);

            /* send data to src port */
            data = get_data_2_send(4'd1);
            command_0x04(4'd1, 4'd1, data);

            /* send datato src port */
            data = get_data_2_send(4'd2);
            command_0x04(4'd2, 4'd2, data);

            /* send data to src port */
            data = get_data_2_send(4'd3);
            command_0x04(4'd3, 4'd3, data);

            /* send data to src port */
            data = get_data_2_send(4'd4);
            command_0x04(4'd4, 4'd4, data);

            /* send data to src port */
            data = get_data_2_send(4'd5);
            command_0x04(4'd5, 4'd5, data);

            /* send data to src port */
            data = get_data_2_send(4'd6);
            command_0x04(4'd6, 4'd6, data);

            /* send data to src port */
            data = get_data_2_send(4'd7);
            command_0x04(4'd7, 4'd7, data);
        endtask

        task send_data_to_all_to_dest_port(input logic [3:0] dest_port);
            logic [array_size-1:0][DW_G-1:0] data;
            set_data_2_send(4'd0, "he");
            set_data_2_send(4'd1, "ll");
            set_data_2_send(4'd2,"gu");
            set_data_2_send(4'd3,"ys");
            set_data_2_send(4'd4,"ia");
            set_data_2_send(4'd5,"m");
            set_data_2_send(4'd6,"ad");
            set_data_2_send(4'd7,"og");

            /* send data to src port */
            data = get_data_2_send(4'd0);
            command_0x04(4'd0, dest_port, data);

            /* send data to src port */
            data = get_data_2_send(4'd1);
            command_0x04(4'd1, dest_port, data);

            /* send datato src port */
            data = get_data_2_send(4'd2);
            command_0x04(4'd2, dest_port, data);

            /* send data to src port */
            data = get_data_2_send(4'd3);
            command_0x04(4'd3, dest_port, data);

            /* send data to src port */
            data = get_data_2_send(4'd4);
            command_0x04(4'd4, dest_port, data);

            /* send data to src port */
            data = get_data_2_send(4'd5);
            command_0x04(4'd5, dest_port, data);

            /* send data to src port */
            data = get_data_2_send(4'd6);
            command_0x04(4'd6, dest_port, data);

            /* send data to src port */
            data = get_data_2_send(4'd7);
            command_0x04(4'd7, dest_port, data);
	        
	        command_0x05();
	        
	        command_0x02();
	        
	        command_0x06();
	        
        endtask

        task fill_all_oports();
            int i,j,k;
            logic [array_size-1:0][DW_G-1:0] data;

            set_data_2_send(4'd0, "he");
            set_data_2_send(4'd1, "llo");
            set_data_2_send(4'd2,"gu");
            set_data_2_send(4'd3,"ys");
            set_data_2_send(4'd4,"ia");
            set_data_2_send(4'd5,"m");
            set_data_2_send(4'd6,"ad");
            set_data_2_send(4'd7,"og");

            for(i=0; i < 8; i++) begin
                k=7;
                /* send data to src port */
                data = get_data_2_send(4'(i));
                for(j=0; j < 8; j++) begin
                    command_0x04(4'(j), 4'(k), data);
                    k -= 1;
                end
            end
        endtask

    endclass
endpackage 
`endif