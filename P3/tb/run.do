if [file exists work] {vdel -all}
vlib work
vlog +define+IF_SIM -f files.f 
onbreak {resume}
set NoQuitOnFinish 1
vsim -voptargs=+acc work.p3_tb
do wave.do
run 700 ms