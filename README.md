# ***Repositorio DVVSD Primavera 2021***

**Desarrolladores:**
- César Villarreal ie707560 @cv4497
- Luis F. Rodríguez ie705694 @LF-RoGu

**Descripción del repositorio:** 
- Este repositorio contiene las práticas, y tareas de la clase de Diseño, Verificación, y Validación de Sistemas Digitales del semestre Primavera 2021

**Índice**
1. [P1](https://gitlab.com/cv4497/ie707560_705694/-/tree/master/P1): Práctica I
2. [P2](https://gitlab.com/cv4497/ie707560_705694/-/tree/master/P2): Práctica II
3. [P3](https://gitlab.com/cv4497/ie707560_705694/-/tree/master/P1): Práctica III

**Tarjeta de Desarrollo**
- [DE-10 Standard](https://www.intel.com/content/dam/altera-www/global/en_US/portal/dsn/42/doc-us-dsnbk-42-5505271707235-de10-standard-user-manual-sm.pdf): tarjeta de desarrollo.

**FPGA**
- [5CSXFC6D6F31C6N SoC](https://www.mouser.mx/datasheet/2/612/cv_51002-1709862.pdf): modelo del FPGA.
- [Cyclone V](https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/hb/cyclone-v/cv_5v1.pdf): familia del FPGA.
