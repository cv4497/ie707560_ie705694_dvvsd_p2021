import data_pkg::*;

module FPGA_uart_tx
(
	input bit clk,
	input logic rst,
	input logic transmit,			//button start tx
	input  data_n_t data_rx, //parallel data from uart2uart
	
	output logic cts,
	output logic serial_rx2tx
);
	

logic o_clk_tx;
clk_divider
#(
	.FREQUENCY(uartTxBR),
	.REFERENCE_CLOCK(uartRefClk)
)
uartTx_clk_divider
(
	.clk_FPGA(clk),
	.rst(rst),
	.clk(o_clk_tx)
);

uart_tx
uart_tx_top
(
	.clk(o_clk_tx),
	.rst(rst),
	.data(data_rx),
	
	.transmit(transmit),
	.cts(cts),
	
	.serial_tx(serial_rx2tx)
);


endmodule 
