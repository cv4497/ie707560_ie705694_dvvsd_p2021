module sipo_register_msb 
#(
	parameter DW = data_pkg::N
) 
(
	input bit clk,
	input logic rst,
	input logic enb,
	input logic data_in,
	output logic [DW-1:0] data_out
);

logic [DW-1:0] rgstr_r;

always_ff@(posedge clk or negedge rst) begin: rgstr_label
    if(!rst)
        rgstr_r <= '0;
    else if (enb)
        rgstr_r <= {data_in, rgstr_r[DW-1:1]};
end:rgstr_label

assign data_out = rgstr_r;

endmodule
