/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: uartTx_fsm
    Description: uartTx_fsm source file
    Last modification: 07/03/2021
*/
import data_pkg::*;

module fsm_delay
(
	input bit clk,
	input bit rst,
	input logic i_data,
	output logic o_data
);

delay_st current_st, nxt_state;

always_comb begin
   case(current_st)
		ST_1: begin
			if(i_data)
				nxt_state = ST_2;
			else 
				nxt_state = ST_1;
		end
		ST_2: begin
			nxt_state = ST_3;
		end
		ST_3: begin
			nxt_state = ST_4;
		end
		ST_4: begin
			nxt_state = ST_5;
		end
		ST_5: begin
			nxt_state = ST_1;
		end
	endcase
end

always_ff@(posedge clk or negedge rst) begin // Circuito Secuenicial en un proceso always.
   if (!rst) 
      current_st  <= ST_1;
   else 
      current_st  <= nxt_state;
end

always_comb begin
   case(current_st)
		ST_1: begin
			o_data = 1'b0;
		end
		ST_2: begin
			o_data = 1'b1;
		end
		ST_3: begin
			o_data = 1'b1;
		end
		ST_4: begin
			o_data = 1'b1;
		end
		ST_5: begin
			o_data = 1'b1;
		end
	endcase
end

endmodule 