module uart_rx
import data_pkg::*;
(
	input	 clk,
	input	 clk_t,
	input	logic rst,
	input logic serial_rx,
	input logic cts,
	
	output logic rts,
	output logic pairty_error,
	output data_n_t data
);

logic bin_cntr_enable;
logic bin_cntr_ovf;
logic enable_w;

logic start_cntr_enable;
logic start_cntr_ovf, start_cntr_ovf_w;
	
logic data_cntr_enable;
logic data_cntr_ovf, data_cntr_ovf_w;

fsm_cntrl_rx
fsm_cntrl_rx_top
(
	.clk(clk),
	.rst(rst),
	.ovf(bin_cntr_ovf),
	.data_in(serial_rx),
	
	.start_cntr_ovf(start_cntr_ovf_w),
	.data_cntr_ovf(data_cntr_ovf),
	
	.rts(rts),
	
	.enable(enable_w),
	.bin_cntr_enb(bin_cntr_enable),
	.start_cntr_enable(start_cntr_enable),
	.data_cntr_enable(data_cntr_enable)
);

sipo_register_msb
sipo_register_msb_uartRX
(
	.clk(clk),
	.rst(rst),
	.enb(enable_w),
	.data_in(serial_rx),
	.data_out(data)
);

bin_counter
#(
	.DW(N),
	.OVF_VAL(N+1)
)
bin_counter_uartRX
(
	 .clk(clk),
    .rst(rst),
    .enb(bin_cntr_enable),
    .ovf(bin_cntr_ovf),
    .count()
);
	

bin_counter
#(
	.DW(N),
	.OVF_VAL(2)
)
start_counter_uartRx
(
	 .clk(clk_t),
    .rst(rst),
    .enb(start_cntr_enable),
    .ovf(start_cntr_ovf),
    .count()
);

fsm_delay
delay_start
(
	.clk(clk_t),
	.rst(rst),
	.i_data(start_cntr_ovf),
	.o_data(start_cntr_ovf_w)
);

bin_counter
#(
	.DW(N),
	.OVF_VAL(2)
)
data_counter_uartRx
(
	 .clk(clk_t),
    .rst(rst),
    .enb(data_cntr_enable),
    .ovf(data_cntr_ovf),
    .count()
);

fsm_delay
delay_data
(
	.clk(clk_t),
	.rst(rst),
	.i_data(data_cntr_ovf),
	.o_data(data_cntr_ovf_w)
);

uart_parity parity_module
(
	.i_data(data),
	.parity(pairty_error)
);

endmodule 