import data_pkg::*;

module FPGA_uart_rx
(
	input bit clk,
	input logic rst,
	input logic serial_tx2rx,			//button start tx
	input logic cts,
	
	output data_n_t data_rx, //parallel data from uart2uart
	
	output rx_interrupt,
	output parity_error
);

logic o_clk_rx;
clk_divider
#(
	.FREQUENCY(uartRxBR),
	.REFERENCE_CLOCK(uartRefClk)
)
uartRx_clk_divider
(
	.clk_FPGA(clk),
	.rst(rst),
	.clk(o_clk_rx)
);
uart_rx
uart_rx_top
(
	.clk(o_clk_rx),
	.clk_t(clk),
	.rst(rst),
	
	.serial_rx(serial_tx2rx),

	.cts(cts),
	
	.rts(rx_interrupt),
	
	.pairty_error(parity_error),
	.data(data_rx)
);


endmodule 
