/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: ppc
    Description: ppc source file
    Last modification: 21/04/2021
*/

module mask_logic
import data_pkg::*;
(
	input logic  clk,
	input logic  rst,
	input req_vector_t  grant_vector,
	input req_vector_t  ppc_vector,
	output req_vector_t  mask_logic_vector
);

req_vector_t mask_logic_t;

always_ff @(posedge clk, negedge rst) begin
	if(!rst)
		mask_logic_t <= '0;
	else begin
		mask_logic_t <=  (ppc_vector << 1'b1) & grant_vector;
	end 
end

assign mask_logic_vector = mask_logic_t;

endmodule
