

import data_pkg::*;
module shift_left
(
	input req_vector_t  i_data,
	input req_vector_t  o_data
);

assign o_data = i_data << 1'b1;

endmodule