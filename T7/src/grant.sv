/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: grant
    Description: grant source file
    Last modification: 21/04/2021
*/

import data_pkg::*;
module grant
#(
    parameter CHANNELS = data_pkg::N,
    parameter WEIGHT = 32
)
(
	input  logic clk,
	input  logic rst,
	
	input  logic [CHANNELS-1 : 0] request,
	input  logic [CHANNELS-1 : 0] next_grant,
	input  logic [WEIGHT-1 : 0] weight,
	
	output  logic [CHANNELS-1 : 0] grant
);

localparam WEIGHT_LIMIT = 16;

logic [WEIGHT-1 : 0] s_counter, s_weight;
logic [CHANNELS-1 : 0] s_request;
	
grant_st current_st, nxt_state;
	
always_comb begin
   case(current_st)
		PROCESS_GRANT : begin 
			if(grant != 'b0) begin
				nxt_state = GET_WEIGHT_GRANT;
			end 
			else begin
				nxt_state = PROCESS_GRANT;
			end 
		end
		GET_WEIGHT_GRANT: begin
			nxt_state = COUNT_GRANT;
		end 
		COUNT_GRANT: begin
			if(s_counter >= s_weight) begin
				nxt_state = PROCESS_GRANT;
			end 
			else if(s_counter >= WEIGHT_LIMIT) begin
				nxt_state = PROCESS_GRANT;
			end 
		end 

	endcase
end	

always_ff@(posedge clk, negedge rst) begin
	if (!rst) begin
		current_st = PROCESS_GRANT;
	end 
	else begin
		current_st = nxt_state;
	end 
end 

always_comb begin
   case(current_st)
		PROCESS_GRANT : begin 
			grant = request & next_grant & (~next_grant + 1'b1);
			s_weight = WEIGHT;
			s_counter = 'd2;
		end
		GET_WEIGHT_GRANT: begin
			grant = grant;
			s_weight = WEIGHT;
			s_counter = s_counter;
		end 
		COUNT_GRANT: begin
			s_counter = s_counter + 1'b1;
			s_weight = WEIGHT;
			grant = grant;
		end 

	endcase
end

endmodule