/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: ppc
    Description: ppc source file
    Last modification: 21/04/2021
*/

import data_pkg::*;
module ppc
#(
    parameter PPC_LENGHT = data_pkg::N
)
(
	input  req_vector_t i_data,
	output req_vector_t o_data
);


req_vector_t pp_req_vector, ppc_vector;
genvar index;

generate 
	for(index = 1; index < PPC_LENGHT; index++) begin
		assign ppc_vector[index] = pp_req_vector[index] | ppc_vector[index-1];
	end 
endgenerate 

assign pp_req_vector = i_data;
assign ppc_vector[0] = pp_req_vector[0];

assign o_data = ppc_vector;

endmodule