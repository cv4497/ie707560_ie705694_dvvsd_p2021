onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {PIPO ENTRADA}
add wave -noupdate /tb_wcrrb/uut/pipo_module/clk
add wave -noupdate /tb_wcrrb/uut/pipo_module/rst
add wave -noupdate /tb_wcrrb/uut/pipo_module/enb
add wave -noupdate /tb_wcrrb/uut/pipo_module/i_data
add wave -noupdate /tb_wcrrb/uut/pipo_module/o_data
add wave -noupdate -divider {MASK LOGIC}
add wave -noupdate /tb_wcrrb/uut/mask_logic_module/grant_vector
add wave -noupdate /tb_wcrrb/uut/mask_logic_module/ppc_vector
add wave -noupdate /tb_wcrrb/uut/mask_logic_module/mask_logic_vector
add wave -noupdate /tb_wcrrb/uut/mask_logic_module/mask_logic_t
add wave -noupdate -divider MUX
add wave -noupdate /tb_wcrrb/uut/mux_2_1_module/i_a
add wave -noupdate /tb_wcrrb/uut/mux_2_1_module/i_b
add wave -noupdate /tb_wcrrb/uut/mux_2_1_module/i_sel
add wave -noupdate /tb_wcrrb/uut/mux_2_1_module/o_sltd
add wave -noupdate -divider PPC
add wave -noupdate /tb_wcrrb/uut/ppc_module/i_data
add wave -noupdate /tb_wcrrb/uut/ppc_module/o_data
add wave -noupdate /tb_wcrrb/uut/ppc_module/pp_req_vector
add wave -noupdate /tb_wcrrb/uut/ppc_module/ppc_vector
add wave -noupdate -divider {PIPO PPC}
add wave -noupdate /tb_wcrrb/uut/ppc_pipo_module/clk
add wave -noupdate /tb_wcrrb/uut/ppc_pipo_module/rst
add wave -noupdate /tb_wcrrb/uut/ppc_pipo_module/enb
add wave -noupdate /tb_wcrrb/uut/ppc_pipo_module/i_data
add wave -noupdate /tb_wcrrb/uut/ppc_pipo_module/o_data
add wave -noupdate -divider {ONE SHOT}
add wave -noupdate /tb_wcrrb/uut/thermometer2oneshot/i_data
add wave -noupdate /tb_wcrrb/uut/thermometer2oneshot/o_data
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {17007 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 348
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {3150 ns}
